package com.yah2de ;
import com.yah2de.Unit;
import openfl.display.Shape;
import openfl.display.Graphics;
import openfl.display.Sprite;
import openfl.geom.Point;
import openfl.display.BitmapData;
import openfl.geom.Rectangle;

/**
 * ...
 * @author Yanrishatum
 */
class Scene extends Unit
{

  public var camera:Point = new Point();
  public var sprite:Sprite;
  public var spriteMask:Shape;
  public var gui:Sprite;
  
  public function new() 
  {
    gui = new Sprite();
    sprite = new Sprite();
    spriteMask = new Shape();
    sprite.addChild(spriteMask);
    super();
    scene = this;
    visible = false;
  }
  
  override public function update():Void 
  {
    if (scene == null) scene = this;
    sprite.x = this.x;
    sprite.y = this.y;
    gui.x = this.x;
    gui.y = this.y;
    super.update();
  }
  
  override public function addedToScene():Void 
  {
    var nearestScene:Scene = scene;
    var u:Unit = parent;
    while (u != null)
    {
      if (Std.is(u, Scene))
      {
        nearestScene = cast u;
        break;
      }
      u = u.parent;
    }
    nearestScene.sprite.addChild(this.sprite);
    nearestScene.gui.addChild(this.gui);
    super.addedToScene();
  }
  
  override public function removedFromScene():Void 
  {
    if (this.sprite.parent == scene.sprite) scene.sprite.removeChild(this.sprite);
    if (this.gui.parent == scene.gui) scene.gui.removeChild(this.gui);
    super.removedFromScene();
    var u:Unit = _childFirst;
    while (u != null)
    {
      u.scene = this;
      u.addedToScene();
      u = u.next;
    }
  }
  
  public function activated():Void
  {
    
  }
  
  public function deactivated():Void
  {
    
  }
  
  @:access(com.yah2de.GE.boxCollider)
  @:access(com.yah2de.GE.boxUnit)
  public function collideRect(rect:Rectangle, colliders:Array<String>, ?mode:CollideMode, z:Float = 0):Unit
  {
    if (mode == null) mode = CollideMode.Both;
    if (colliders.length == 0) return null;
    
    GE.boxCollider.x = rect.x;
    GE.boxCollider.y = rect.y;
    GE.boxCollider.z = z;
    GE.boxCollider.width = rect.width;
    GE.boxCollider.height = rect.height;
    GE.boxUnit.collidesWith = colliders;
    var units:Array<Unit> = getByTypes(colliders);
    for (unit in units)
    {
      if (unit.collides(GE.boxUnit, mode)) return unit;
    }
    return null;
  }
  
  public function getCollision(u:Unit, x:Float, y:Float, z:Float, types:Array<String> = null, ?mode:CollideMode):Unit
  {
    if (mode == null) mode = CollideMode.Both;
    if (types == null) types = u.collidesWith;
    if (types.length == 0) return null;
    
    var bx:Float = u.x;
    var by:Float = u.y;
    var bz:Float = u.z;
    u.x = x;
    u.y = y;
    u.z = z;
    
    var possibleCollisions:Array<Unit> = getByTypes(types);
    for (i in possibleCollisions)
    {
      if (u != i && u.collides(i, mode))
      {
        u.x = bx;
        u.y = by;
        u.z = bz;
        return i;
      }
    }
    
    u.x = bx;
    u.y = by;
    u.z = bz;
    return null;
  }
  
  public function getCollisions(u:Unit, x:Float, y:Float, z:Float, types:Array<String> = null, to:Array<Unit> = null):Array<Unit>
  {
    var bx:Float = u.x;
    var by:Float = u.y;
    var bz:Float = u.z;
    u.x = x;
    u.y = y;
    u.z = z;
    
    if (to == null) to = new Array();
    if (types == null) types = u.collidesWith;
    
    var possibleCollisions:Array<Unit> = getByTypes(types);
    for (i in possibleCollisions)
    {
      if (u.collides(i)) to.push(i);
    }
    
    u.x = bx;
    u.y = by;
    u.z = bz;
    return to;
  }
  
}