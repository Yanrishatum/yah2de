package com.yah2de ;

/**
 * ...
 * @author Yanrishatum
 */
enum RenderPriorityMode
{

  /**
   * In Hierarchy render mode, Unit childs will be rendered right after parent Unit.
   * DO NOT WORK AS EXPECTED, NOT FIXED BY NOW
   */
  Hierarchy;
  /**
   * In Heap render mode, all Units will be sorted by real W and real Y coordinates, ignoring parenting of Units.
   * doRenderChilds still will be checked.
   */
  Heap;
  /**
   * In HeapRealY render mode, all Units will be sorted by lcaol W and real Y coordinates (e.g. Unit does not inherit W from parent Units)
   * doRenderChilds still will be checked.
   */
  HeapRealY;
}