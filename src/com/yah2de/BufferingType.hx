package com.yah2de;

/**
 * @author Yanrishatum
 */

enum BufferingType 
{
  SingleBuffer;
  DoubleBuffer;
  TripleBuffer;
}