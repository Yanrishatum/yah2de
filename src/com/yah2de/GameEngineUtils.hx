package com.yah2de ;
import com.yah2de.utils.MacroUtils;
import openfl.geom.Point;
import openfl.geom.Rectangle;

typedef GEUtils = GameEngineUtils;

typedef XYObject =
{
  var x:Float;
  var y:Float;
}

typedef WObject =
{
  var w:Float;
}

/**
 * ...
 * @author Yanrishatum
 */
class GameEngineUtils
{

  public static inline var PI:Float = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679821480865132823066470938446095505822317253594081284811174502841027019385211055596446229489549303819644288109756659334461284756482337867831652712019091456;
  public static inline var HALF_PI:Float = PI * 0.5;
  public static inline var QUART_PI:Float = HALF_PI * 0.5;
  public static inline var EIGHT_PI:Float = QUART_PI * 0.5;
  public static inline var PI2:Float = PI + PI;
  public static inline var NPI:Float = -PI;
  public static inline var DEGREES_TO_RADIANS:Float = PI / 180; // Not sure, as always forgot formula
  public static inline var RADIANS_TO_DEGRESS:Float = 180 / PI;
  
  public static inline function clearArray<T>(arr:Array<T>):Void
  {
    while (arr.length > 0) { arr.pop(); }
  }
  
  public static inline function clampi(val:Int, max:Int, min:Int):Int
  {
    if (val > max) return max;
    else if (val < min) return min;
    else return val;
  }
  
  public static inline function clamp(val:Float, max:Float, min:Float):Float
  {
    if (max < min) MacroUtils.swap(max, min);
    if (val > max) return max;
    else if (val < min) return min;
    else return val;
  }
  
  public static inline function inCamera(p:Point, width:Int, height:Int, camera:Point):Bool
  {
    return p.x + width >= camera.x &&
           p.y + height >= camera.y &&
           p.x < camera.x + GE.width &&
           p.y < camera.y + GE.height;
  }
  
  public static inline function inRect(x:Float, y:Float, rx:Float, ry:Float, rw:Float, rh:Float):Bool
  {
    return x >= rx && x < rx + rw && y >= ry && y < ry + rh;
  }
  
  public static inline function pointAt(x:Float, y:Float):Point
  {
    GE.point.setTo(x, y);
    return GE.point;
  }
  
  public static inline function rectAt(x:Float, y:Float, width:Float, height:Float):Rectangle
  {
    GE.rect.setTo(x, y, width, height);
    return GE.rect;
  }
  
  public static inline function angle(angle:Float, length:Float = 1, to:XYObject):XYObject
  {
    if (to == null) to == GE.point;
    to.x = Math.cos(angle) * length;
    to.y = Math.sin(angle) * length;
    return to;
  }
  
  public static inline function angleDegree(angle:Float, length:Float = 1, to:XYObject):XYObject
  {
    return GameEngineUtils.angle(angle * DEGREES_TO_RADIANS, length, to);
  }
  
  public static inline function isBetween(value:Float, min:Float, max:Float):Bool
  {
    if (min > max) MacroUtils.swap(min, max);
    return value >= min && value <= max;
  }
  
  public static inline function square(v:Float):Float
  {
    return v * v;
  }
  
  public static inline function angleToDirection(angle:Float, directions:Int):Int
  {
    return Math.round(directions * angle / PI2 + directions) % directions;
  }
  
  public static inline function directionToAngle(dir:Int, directions:Int):Float
  {
    return dir / directions * PI2; // 1 / 4 = 0.25 * PI2 = HalfPi
  }
  
  public static inline function applyHomingAt(angle:Float, root:Unit, target:Unit, homingValue:Float, applyElapsed:Bool = true):Float
  {
    return applyHoming(angle, Math.atan2(target.realY - root.realY, target.realX - root.realX), homingValue, applyElapsed);
  }
  
  public static function applyHoming(angle:Float, targetAngle:Float, homingValue:Float, applyElapsed:Bool = true):Float
  {
    if (angle > PI) angle -= PI2;
    if (angle < NPI) angle += PI2;
    var diff:Float = targetAngle - angle;
    if (diff > PI) diff -= PI2;
    if (diff < NPI) diff += PI2;
    
    if (applyElapsed) homingValue *= GE.elapsed;
    if (homingValue > Math.abs(diff)) homingValue = Math.abs(diff);
    
    if (diff > 0) return angle + homingValue;
    else return angle - homingValue;
    
    /*if (angle > PI) angle -= PI2;
    if (angle < NPI) angle += PI2;
    var diff:Float = targetAngle - angle;
    if (diff < PI) diff -= PI2;
    if (diff > NPI) diff += PI2;
    
    var out:Float = angle;
    if (diff > 0)
    {
      out += (applyElapsed ? (homingValue * GE.elapsed) : homingValue);
      if (targetAngle - out < 0) return targetAngle;
      else return out;
    }
    else if (diff < 0)
    {
      out += (applyElapsed ? (homingValue * GE.elapsed) : homingValue);
      if (targetAngle - out > 0) return targetAngle;
      else return out;
    }
    else return out;
    return out;*/
  }
  
  public static function filterInactive<T:Unit>(arr:Array<T>):Array<T>
  {
    return arr.filter(inactiveFilter);// Lambda.filter(arr, inactiveFilter);
  }
  
  private static function inactiveFilter(u:Unit):Bool
  {
    return u.active;
  }
  
  /**
   * Converts global coordinates to camera coordinates.
   * Note: Uses GE.point
   * @param p
   * @param camera
   * @return GE.point
   */
  public static inline function toCameraCoords(p:Point, camera:Point):Point
  {
    GE.point.setTo(p.x - camera.x, p.y - camera.y);
    return GE.point;
  }
  
  public static inline function min(a:Int, b:Int):Int
  {
    return a > b ? b : a;
  }
  
  public static inline function max(a:Int, b:Int):Int
  {
    return a > b ? a : b;
  }
  
  public static inline function distance(fromX:Float, fromY:Float, toX:Float, toY:Float):Float
  {
    return Math.sqrt(square(toX - fromX) + square(toY - fromY));
  }
  
  public static function wSort(a:WObject, b:WObject):Int
  {
    if (a.w > b.w) return -1;
    else if (a.w < b.w) return 1;
    return 0;
  }
  
  /**
   * Sorts in reverse W order. Higher W value will be last.
   */
  public static function reverseWSort(a:WObject, b:WObject):Int
  {
    if (a.w > b.w) return 1;
    else if (a.w < b.w) return 1;
    return 0;
  }
  
  //==========================================================
  //{ HaxePunk code sector
  //==========================================================
  
	/**
	 * Optimized version of Lambda.indexOf for Array on dynamic platforms (Lambda.indexOf is less performant on those targets).
	 *
	 * @param	arr		The array to look into.
	 * @param	param	The value to look for.
	 * @return	Returns the index of the first element [v] within Array [arr].
	 * This function uses operator [==] to check for equality.
	 * If [v] does not exist in [arr], the result is -1.
	 **/
	public static inline function indexOf<T>(arr:Array<T>, v:T) : Int
	{
		#if (haxe_ver >= 3.1) 
		return arr.indexOf(v);
		#else
			#if (flash || js)
			return untyped arr.indexOf(v);
			#else
			return std.Lambda.indexOf(arr, v);
			#end
		#end
	}
  
  //==========================================================
  //}
  //==========================================================
}