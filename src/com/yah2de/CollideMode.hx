package com.yah2de;

/**
 * ...
 * @author Yanrishatum
 */
enum CollideMode
{

  Trigger;
  Physics;
  Both;
  
}