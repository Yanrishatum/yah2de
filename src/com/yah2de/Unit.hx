package com.yah2de ;
import openfl.display.BitmapData;
import openfl.display.Graphics;
import openfl.geom.Point;
import com.yah2de.Screen;
import openfl.geom.Rectangle;

/**
 * ...
 * @author Yanrishatum
 */
class Unit
{
  public var name:String;
  public var types:Array<String> = new Array();
  
  public var x:Float = 0;
  public var y:Float = 0;
  public var z:Float = 0;
  public var w:Float = 0; // Render priority
  public var priority:Int = 0; // Update priority
  public var parent:Unit;
  public var scene:Scene;
  
  public var zCollideThreshold:Float = 1;
  
  /**
   * Is this Unit active and requires updating?
   */
  public var active:Bool = true;
  /**
   * Is this Unit visible and will be rendered?
   */
  public var visible:Bool = true;
  /**
   * Is childs of this Unit requires updating?
   */
  public var doUpdateChilds:Bool = true;
  /**
   * Is childs of this Unit requires rendering?
   */
  public var doRenderChilds:Bool = true;
  
  // Used for enable/disable
  private var __active:Bool = true;
  private var __visible:Bool = true;
  private var __doUpdateChilds:Bool = true;
  private var __doRenderChilds:Bool = true;
  private var __collidable:Bool = true;
  private var __childsCollidable:Bool = true;
  
  public var collidable:Bool = true;
  public var childsCollidable:Bool = true;
  public var colliders:Array<Collider> = new Array();
  public var collidesWith:Array<String> = new Array();
  
  public var canCollide(get, never):Bool;
  private inline function get_canCollide():Bool { return collidable && (parent != null ? parent.get_canChildsCollide() : true); }
  
  public var canChildsCollide(get, never):Bool;
  private inline function get_canChildsCollide():Bool
  {
    return childsCollidable && (parent != null ? parent.get_canChildsCollide() : true);
  }
  
  public var prev:Unit;
  public var next:Unit;
  public var updateNext:Unit;
  public var renderNext:Unit;
  
  @:allow(com.yah2de.Screen)
  private var _childFirst:Unit;
  private var _childLast:Unit;
  private var _updateFirst:Unit;
  private var _renderFirst:Unit;
  
  private var _childsByType:Map<String, Array<Unit>> = new Map();
  
  public var flag_sortUpdatePriority:Bool; // Set this flag to true, when update priority of children are changed
  
  public var realX(get, never):Float;
  private function get_realX():Float
  {
    if (parent != null) return this.x + parent.get_realX();
    else return this.x;
  }
  
  public var realY(get, never):Float;
  private function get_realY():Float
  {
    if (parent != null) return this.y + parent.get_realY();
    else return this.y;
  }
  
  public var realZ(get, never):Float;
  private function get_realZ():Float
  {
    if (parent != null) return this.z + parent.get_realZ();
    else return this.z;
  }
  
  public var realW(get, never):Float;
  private function get_realW():Float
  {
    if (parent != null) return this.w + parent.get_realW();
    else return this.w;
  }
  
  public function new() 
  {
    
  }
  
  public function disable(andChilds:Bool = false, saveStateForEnable:Bool = true):Void
  {
    if (active != false || visible != false || collidable != false)
    {
      if (saveStateForEnable)
      {
        this.__active = active;
        this.__visible = visible;
        this.__doRenderChilds = doRenderChilds;
        this.__doUpdateChilds = doUpdateChilds;
        this.__collidable = collidable;
        this.__childsCollidable = childsCollidable;
      }
    }
    
    active = false;
    visible = false;
    collidable = false;
    if (andChilds)
    {
      doRenderChilds = false;
      doUpdateChilds = false;
      childsCollidable = false;
    }
  }
  
  public function enable(andChilds:Bool = true, useValuesBeforeDisable:Bool = true):Void
  {
    if (useValuesBeforeDisable)
    {
      active = this.__active;
      visible = this.__visible;
      collidable = this.__collidable;
      if (andChilds)
      {
        doRenderChilds = this.__doRenderChilds;
        doUpdateChilds = this.__doUpdateChilds;
        childsCollidable = this.__childsCollidable;
      }
    }
    else
    {
      active = true;
      visible = true;
      collidable = true;
      if (andChilds)
      {
        doRenderChilds = true;
        doUpdateChilds = true;
        childsCollidable = true;
      }
    }
  }
  
  //{ Add/Remove childs
  
  private var _removeList:Array<Unit> = new Array();
  private var _addList:Array<Unit> = new Array();
  
  public function addChild(u:Unit, instant:Bool = false):Void
  {
    if (!instant) 
    {
      _addList.push(u);
      return;
    }
    if (u.parent != null)
    {
      if (u.parent == this) return; // Already added
      u.parent.removeChild(u, instant);
    }
    
    u.next = null;
    u.prev = null;
    u.parent = this;
    if (_childLast != null)
    {
      _childLast.next = u;
      u.prev = _childLast;
      _childLast = u;
    }
    else
    {
      _childFirst = _childLast = u;
    }
    __addUnitTreeToTypes(u);
    u.added();
    if (scene != null)
    {
      u.scene = scene;
      u.addedToScene();
    }
    flag_sortUpdatePriority = true;
  }
  
  public function removeChild(u:Unit, instant:Bool = false):Bool
  {
    if (!instant)
    {
      _removeList.push(u);
      return false;
    }
    if (u.parent == this)
    {
      if (u == _childLast)
      {
        _childLast = u.prev;
        if (_childLast != null) _childLast.next = null;
        u.prev = null;
      }
      if (u == _childFirst)
      {
        _childFirst = u.next;
        u.next = null;
      }
      
      if (u.prev != null) u.prev.next = u.next;
      if (u.next != null) u.next.prev = u.prev;
      u.next = null;
      u.prev = null;
      u.updateNext = null;
      u.removed();
      if (u.scene != null) u.removedFromScene();
      __removeUnitTreeFromTypes(u);
      u.parent = null;
      u.scene = null;
      
      flag_sortUpdatePriority = true;
      return true;
    }
    return false;
  }
  
  public function addChilds(units:Array<Unit>, instant:Bool = false):Void
  {
    for (u in units) addChild(u, instant);
  }
  
  public function removeChilds(units:Array<Unit>, instant:Bool = false):Void
  {
    for (u in units) removeChild(u, instant);
  }
  
  public function processPendingChilds():Void
  {
    if (_addList.length > 0)
    {
      for (u in _addList) addChild(u, true);
      GameEngineUtils.clearArray(_addList);
    }
    if (_removeList.length > 0)
    {
      for (u in _removeList) removeChild(u, true);
      GameEngineUtils.clearArray(_removeList);
    }
  }
  
  public function addCollider<T:Collider>(c:T):T
  {
    if (c.parent == this) return c;
    if (c.parent != null) c.parent.removeCollider(c);
    c.parent = this;
    colliders.push(c);
    c.added();
    return c;
  }
  
  public function removeCollider(c:Collider):Bool
  {
    if (colliders.remove(c))
    {
      c.removed();
      c.parent = null;
      return true;
    }
    return false;
  }
  
  public function addType(type:String):Void
  {
    if (GEU.indexOf(types, type) == -1)
    {
      types.push(type);
      if (parent != null)
      {
        parent.__addUnitToType(type, this);
      }
    }
  }
  
  public function removeType(type:String):Bool
  {
    if (types.remove(type))
    {
      if (parent != null) parent.__removeUnitFromType(type, this);
      return true;
    }
    return false;
  }
  
  private function __addUnitToType(type:String, unit:Unit):Void
  {
    var arr:Array<Unit> = _childsByType.get(type);
    if (arr == null) _childsByType.set(type, arr = new Array<Unit>());
    arr.push(unit);
    if (parent != null) parent.__addUnitToType(type, unit);
  }
  
  private function __removeUnitFromType(type:String, unit:Unit):Void
  {
    var arr:Array<Unit> = _childsByType.get(type);
    if (arr != null)
    {
      arr.remove(unit);
    }
    if (parent != null) parent.__removeUnitFromType(type, unit);
  }
  
  private function __addUnitsToType(type:String, units:Array<Unit>):Void
  {
    var arr:Array<Unit> = _childsByType.get(type);
    if (arr == null)
    {
      _childsByType.set(type, units.copy());
      if (parent != null) parent.__addUnitsToType(type, units);
      return;
    }
    for (u in units) arr.push(u);
    if (parent != null) parent.__addUnitsToType(type, units);
  }
  
  private function __removeUnitsFromType(type:String, units:Array<Unit>):Void
  {
    var arr:Array<Unit> = _childsByType.get(type);
    if (arr != null) for (u in units) arr.remove(u);
    if (parent != null) parent.__removeUnitsFromType(type, units);
  }
  
  private function __addUnitTreeToTypes(root:Unit):Void
  {
    for (type in root.types)
    {
      __addUnitToType(type, root);
    }
    var u:Unit = root._childFirst;
    while (u != null)
    {
      __addUnitTreeToTypes(u);
      u = u.next;
    }
  }
  
  private function __removeUnitTreeFromTypes(root:Unit):Void
  {
    for (type in root.types)
    {
      __removeUnitFromType(type, root);
    }
    var u:Unit = root._childFirst;
    while (u != null)
    {
      __removeUnitTreeFromTypes(u);
      u = u.next;
    }
  }
  
  //}
  
  //{ Search, checking
  
  // Check this unit
  
  public inline function hasType(type:String):Bool
  {
    return GEU.indexOf(types, type) != -1;// Lambda.has(types, type);
  }
  
  public function isOneOf(types:Array<String>):Bool
  {
    for (t in types)
    {
      for (myT in this.types)
      {
        if (t == myT) return true;
      }
    }
    return false;
  }
  
  // Check child units
  
  // By type
  
  public function getNearestType(type:String, x:Float, y:Float, z:Float):Unit
  {
    var units:Array<Unit> = _childsByType.get(type);
    if (units != null)
    {
      var nearest:Unit = null;
      var nearestDist:Float = Math.POSITIVE_INFINITY;
      for (u in units)
      {
        var dist:Float = GEU.square(u.realX - x) + GEU.square(u.realY - y) + GEU.square(u.realZ - z);
        if (dist < nearestDist)
        {
          nearestDist = dist;
          nearest = u;
        }
      }
      return nearest;
    }
    return null;
  }
  
  public function getInRadius(x:Float, y:Float, z:Float, radius:Float, types:Array<String>, to:Array<Unit> = null):Array<Unit>
  {
    // TODO: Rebuild for new type system
    if (to == null) to = new Array();
    if (types == null) return to; // Nothing to check
    radius = radius * radius; // Squaring
    var u:Unit = _childFirst;
    while (u != null)
    {
      if (u.isOneOf(types) && GEU.square(u.realX - x) + GEU.square(u.realY - y) + GEU.square(u.realZ - z) <= radius) to.push(u);
      u.getInRadius(x, y, z, radius, types, to);
      u = u.next;
    }
    return to;
  }
  
  public function getInRadiusT<T:Unit>(x:Float, y:Float, z:Float, radius:Float, types:Array<String>, cl:Class<T>, to:Array<T> = null):Array<T>
  {
    // Primitive algorithm, checks only origin position
    if (to == null) to = new Array();
    if (types == null) return to; // Nothing to check
    radius = radius * radius; // Squaring
    var u:Unit = _childFirst;
    while (u != null)
    {
      if (Std.is(u, cl) && u.isOneOf(types) && GEU.square(u.realX - x) + GEU.square(u.realY - y) + GEU.square(u.realZ - z) <= radius) to.push(cast u);
      u.getInRadiusT(x, y, z, radius, types, cl, to);
      u = u.next;
    }
    return to;
  }
  
  public function getByTypes(types:Array<String>, to:Array<Unit> = null):Array<Unit>
  {
    if (to == null) to = new Array();
    for (t in types)
    {
      var arr:Array<Unit> = _childsByType.get(t);
      if (arr != null)
      {
        for (u in arr)
        {
          if (GEU.indexOf(to, u) == -1) to.push(u);
        }
      }
    }
    return to;
  }
  
  public function getByType(type:String, to:Array<Unit> = null):Array<Unit>
  {
    if (to == null) to = new Array();
    
    var types:Array<Unit> = _childsByType.get(type);
    
    if (types == null) return to;
    
    for (u in types) to.push(u);
    
    return to;
  }
  
  public function getByTypeT<T:Unit>(type:String, cl:Class<T>, to:Array<T> = null):Array<T>
  {
    if (to == null) to = new Array();
    var types:Array<Unit> = _childsByType.get(type);
    if (types == null) return to;
    for (t in types)
    {
      if (Std.is(t, cl)) to.push(cast t);
    }
    return to;
  }
  
  public function getTypeCount(type:String):Int
  {
    var types:Array<Unit> = _childsByType.get(type);
    if (types == null) return 0;
    return types.length;
  }
  
  public function getActiveTypeCount(type:String):Int
  {
    var types:Array<Unit> = _childsByType.get(type);
    if (types == null) return 0;
    var i:Int = 0;
    for (u in types) if (u.active) i++;
    return i;
  }
  
  // Name/class
  
  public function getByName(name:String):Unit
  {
    var u:Unit = _childFirst;
    while (u != null)
    {
      if (u.name == name) return u;
      var i:Unit = u.getByName(name);
      if (i != null) return i;
      u = u.next;
    }
    
    return null;
  }
  
  public function getByClass<T:Unit>(cl:Class<T>, to:Array<T> = null):Array<T>
  {
    if (to == null) to = new Array();
    
    var u:Unit = _childFirst;
    while (u != null)
    {
      if (Std.is(u, cl)) to.push(cast u);
      u.getByClass(cl, to);
      u = u.next;
    }
    return to;
  }
  
  public function getFirst():Unit
  {
    return _childFirst;
  }
  
  public function getAll(to:Array<Unit> = null):Array<Unit>
  {
    if (to == null) to = new Array();
    var u:Unit = _childFirst;
    while (u != null)
    {
      to.push(u);
      u = u.next;
    }
    return to;
  }
  
  // Direct collide check
  
  public function collides(other:Unit, ?mode:CollideMode):Bool
  {
    if (mode == null) mode = CollideMode.Both;
    if (!canCollide || !other.canCollide) return false;
    for (collider in colliders)
    {
      if (collider.satisfiesCollideMode(mode))
      {
        for (o in other.colliders)
        {
          if (collider.canCollideWith(o, mode) && collider.collides(o)) return true;
        }
      }
    }
    return false;
  }
  
  //}
  
  //{ Sorting
  
  public function sortUpdatePriority():Void
  {
    _updateFirst = _childFirst;
    if (_updateFirst == null) return;
    _updateFirst.updateNext = null;
    var u:Unit = _childFirst.next;
    var p:Unit;
    var i:Unit;
    while (u != null)
    {
      p = null;
      i = _updateFirst;
      u.updateNext = null;
      while (i != null)
      {
        if (u.priority >= i.priority)
        {
          u.updateNext = i;
          if (p == null) _updateFirst = u;
          else p.updateNext = u;
          break;
        }
        p = i;
        i = i.updateNext;
      }
      u = u.next;
    }
    
    flag_sortUpdatePriority = false;
  }
  
  public function sortRenderPriority():Void
  {
    _renderFirst = _childFirst;
    if (_renderFirst == null) return;
    _renderFirst.renderNext = null;
    var u:Unit = _renderFirst.next;
    var p:Unit;
    var i:Unit;
    while (u != null)
    {
      p = null;
      i = _renderFirst;
      u.renderNext = null;
      
      // W render
      while (i != null)
      {
        if (u.w >= i.w)
        {
          break;
        }
        p = i;
        i = i.renderNext;
      }
      
      // Y render
      while (i != null)
      {
        if (i.y >= u.y)
        {
          u.renderNext = i;
          if (p == null) _renderFirst = u;
          else p.renderNext = u;
          break;
        }
        p = i;
        i = i.renderNext;
      }
      
      u = u.next;
    }
  }
  
  //}
  
  //{ Updating (added, removed, update, postUpdate, updateChilds, postUpdateChilds)
  
  public function addedToScene():Void
  {
    var u:Unit = this._childFirst;
    while (u != null)
    {
      u.scene = scene;
      u.addedToScene();
      u = u.next;
    }
  }
  
  public function removedFromScene():Void
  {
    var u:Unit = this._childFirst;
    while (u != null)
    {
      u.removedFromScene();
      u.scene = null;
      u = u.next;
    }
  }
  
  public function added():Void
  {
    
  }
  
  public function removed():Void
  {
    
  }
  
  public function update():Void
  {
    for (c in colliders)
    {
      if (c.needUpdate) c.update();
    }
  }
  
  public function postUpdate():Void
  {
    for (c in colliders)
    {
      if (c.needUpdate) c.postUpdate();
    }
  }
  
  public function updateChilds():Void
  {
    processPendingChilds();
    if (doUpdateChilds)
    {
      if (flag_sortUpdatePriority) sortUpdatePriority();
      var u:Unit = _updateFirst;
      while (u != null)
      {
        if (u.active) u.update();
        if (u.doUpdateChilds) u.updateChilds();
        u = u.updateNext;
      }
    }
  }
  
  public function postUpdateChilds():Void
  {
    processPendingChilds();
    if (doUpdateChilds)
    {
      if (flag_sortUpdatePriority) sortUpdatePriority();
      var u:Unit = _updateFirst;
      while (u != null)
      {
        if (u.active) u.postUpdate();
        if (u.doUpdateChilds) u.postUpdateChilds();
        u = u.updateNext;
      }
    }
  }
  
  //}
  
  //{ Rendering
  
  // IsOnScreen check while rendering?
  
  public function renderSoftware(buffer:BitmapData, camera:Point):Void
  {
  }
  
  public function renderChildsSoftware(buffer:BitmapData, camera:Point):Void
  {
    if (doRenderChilds)
    {
      var u:Unit = _renderFirst;
      while (u != null)
      {
        if (u.visible) u.renderSoftware(buffer, camera);
        if (u.doRenderChilds) u.renderChildsSoftware(buffer, camera);
        u = u.renderNext;
      }
    }
  }
  
  public function renderHardware(g:Graphics, camera:Point, scaleX:Float, scaleY:Float):Void
  {
  }
  
  public function renderChildsHardware(g:Graphics, camera:Point, scaleX:Float, scaleY:Float):Void
  {
    if (doRenderChilds)
    {
      var u:Unit = _renderFirst;
      while (u != null)
      {
        if (u.visible) u.renderHardware(g, camera, scaleX, scaleY);
        if (u.doRenderChilds) u.renderChildsHardware(g, camera, scaleX, scaleY);
        u = u.renderNext;
      }
    }
  }
  
  public function renderColliders(g:Graphics):Void
  {
    if (this.realZ != 0)
    {
      g.lineStyle(0.1, 0xFF00FF);
      g.moveTo(this.realX, this.realY);
      g.lineTo(this.realX, this.realY - this.realZ);
      g.lineStyle();
    }
    else
    {
      g.beginFill(0xFF00FF);
      g.drawRect(this.realX - 0.5, this.realY - 0.5, 1, 1);
      g.endFill();
    }
    for (collider in colliders)
    {
      collider.renderDebug(g);
    }
  }
  
  //}
  
}