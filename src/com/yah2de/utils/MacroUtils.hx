package com.yah2de.utils;
import haxe.macro.Context;
import haxe.macro.Expr;

/**
 * ...
 * @author Yanrishatum
 */
class MacroUtils
{
  
  public static macro function swap(a:Expr, b:Expr):Expr
  {
    return
    { expr:ExprDef.EBlock([
      { expr:ExprDef.EVars([ { name:"tmp", type:null, expr:a } ]), pos:Context.currentPos() },
      { expr:ExprDef.EBinop(Binop.OpAssign, a, b), pos:Context.currentPos() },
      { expr:ExprDef.EBinop(Binop.OpAssign, b, { expr:ExprDef.EConst(Constant.CIdent("tmp")), pos:Context.currentPos() }), pos:Context.currentPos() }
    ]), pos:Context.currentPos() };
  }
  
}