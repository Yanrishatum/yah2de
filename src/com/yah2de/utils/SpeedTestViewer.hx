package com.yah2de.utils;
import haxe.ds.Vector;
import openfl.display.Graphics;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.text.TextField;
import openfl.text.TextFieldAutoSize;
import openfl.text.TextFormat;

/**
 * ...
 * @author ...
 */
class SpeedTestViewer extends Sprite
{
  
  public var updateTime(default, set):Float;
  public var renderTime(default, set):Float;
  private inline function set_updateTime(v:Float):Float
  {
    _updateText.text = "Update time: " + zerofill(Math.floor(v * 1000)) + "ms";
    return updateTime = v;
  }
  
  private inline function set_renderTime(v:Float):Float
  {
    _renderText.text = "Render time: " + zerofill(Math.floor(v * 1000)) + "ms";
    return renderTime = v;
  }
  
  private inline function zerofill(v:Int):String
  {
    var s:String = Std.string(v);
    while (s.length < 4) s = "0" + s;
    return s;
  }
  
  private var _updateText:TextField;
  private var _renderText:TextField;
  
  private var _total:Float;
  private var _pointsTotal:Vector<Float>;
  private var _pointsRender:Vector<Float>;
  private var _pointsUpdate:Vector<Float>;
  
  private var readCaret:Int;
  
  private var _limit:Float;
  
  private static inline var GRAPH_WIDTH:Int = 100;
  private static inline var GRAPH_HEIGHT:Float = 50;
  private static inline var GRAPH_X:Float = 2;
  private static inline var GRAPH_Y:Float = 40;
  private static inline var GRAPH_SCALE:Float = 3;
  
  public function new() 
  {
    super();
    var format:TextFormat = new TextFormat("Courier New", 12, 0xFFFFFF); 
    _updateText = new TextField();
    _renderText = new TextField();
    
    _updateText.defaultTextFormat = format;
    _renderText.defaultTextFormat = format;
    
    _updateText.autoSize = TextFieldAutoSize.LEFT;
    _renderText.autoSize = TextFieldAutoSize.LEFT;
    
    /*
    _updateText.height = 14;
    _renderText.height = 14;
    
    _updateText.width = 150;
    _renderText.width = 150;
    */
    _renderText.y = 16;
    
    addChild(_updateText);
    addChild(_renderText);
    this.y = 2;
    
    _pointsTotal = new Vector(GRAPH_WIDTH);
    _pointsRender = new Vector(GRAPH_WIDTH);
    _pointsUpdate = new Vector(GRAPH_WIDTH);
    for (i in 0...GRAPH_WIDTH)
    {
      _pointsTotal[i] = 0;
      _pointsRender[i] = 0;
      _pointsUpdate[i] = 0;
    }
    readCaret = 0;
    _limit = 0;
    
    this.addEventListener(Event.ENTER_FRAME, onRender);
  }
  
  private function onRender(e:Event):Void
  {
    _total = renderTime + updateTime;
    if (_limit < GE.unscaledElapsed)
    {
      recalculatePoints(_limit, GE.unscaledElapsed);
      _limit = GE.unscaledElapsed;
    }
    
    if (_total > _limit)
    {
      recalculatePoints(_limit, _total);
      _limit = _total;
    }
    readCaret = (readCaret + 1) % GRAPH_WIDTH;
    if (readCaret == 0)
    {
      var newLimit:Float = GE.unscaledElapsed;
      for (i in 0...GRAPH_WIDTH)
      {
        var tmp:Float = _pointsTotal[i] / GRAPH_HEIGHT * _limit;
        if (tmp > newLimit) newLimit = tmp;
      }
      recalculatePoints(_limit, newLimit);
      _limit = newLimit;
    }
    _pointsTotal[readCaret] = _total / _limit * GRAPH_HEIGHT;
    _pointsRender[readCaret] = renderTime / _limit * GRAPH_HEIGHT;
    _pointsUpdate[readCaret] = updateTime / _limit * GRAPH_HEIGHT;
    //trace(_total, _limit, _total / _limit, _total / _limit * GRAPH_HEIGHT, readCaret, _pointsTotal);
    graphics.clear();
    renderGraph(_pointsTotal, 0xFFFF00);
    renderGraph(_pointsRender, 0xFF0000);
    renderGraph(_pointsUpdate, 0x00FF00);
  }
  
  private inline function renderGraph(points:Vector<Float>, color:Int):Void
  {
    var g:Graphics = this.graphics;
    g.lineStyle(0.5, color);
    var renderCaret:Int = (readCaret + 1) % GRAPH_WIDTH;
    
    g.moveTo(GRAPH_X, (GRAPH_Y + (GRAPH_HEIGHT - points[renderCaret])) * GRAPH_SCALE);
    
    renderCaret++;
    if (renderCaret == GRAPH_WIDTH) renderCaret = 0;
    for (i in 1...GRAPH_WIDTH)
    {
      g.lineTo((GRAPH_X + i) * GRAPH_SCALE, (GRAPH_Y + (GRAPH_HEIGHT - points[renderCaret])) * GRAPH_SCALE);
      renderCaret++;
      if (renderCaret == GRAPH_WIDTH) renderCaret = 0;
    }
  }
  
  private inline function recalculatePoints(prevLimit:Float, newLimit:Float):Void
  {
    if (prevLimit != 0)
    for (i in 0...GRAPH_WIDTH)
    {
      _pointsTotal[i] = _pointsTotal[i] / prevLimit * newLimit;
      _pointsRender[i] = _pointsRender[i] / prevLimit * newLimit;
      _pointsUpdate[i] = _pointsUpdate[i] / prevLimit * newLimit;
    }
  }
  
}