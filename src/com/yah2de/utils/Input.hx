package com.yah2de.utils;

// This class from HaxePunk (slightly edited)

import com.yah2de.GE;
import com.yah2de.GEU;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.events.TouchEvent;
import flash.ui.Keyboard;
import flash.ui.Multitouch;
import flash.ui.MultitouchInputMode;

#if (cpp || neko)
	import openfl.events.JoystickEvent;
#end

#if ouya
import tv.ouya.console.api.OuyaController;
import openfl.utils.JNI;
#end

class Input
{

	private static inline var kKeyStringMax = 100;

	private static var _enabled:Bool;
	private static var _touchNum:Int;
	private static var _key:Map<Int, Bool>;
	private static var _keyNum:Int;
	private static var _press:Array<Int>;
	private static var _pressNum:Int;
  private static var _textPress:Array<Int>;
  private static var _textPressNum:Int;
	private static var _release:Array<Int>;
	private static var _releaseNum:Int;
	private static var _mouseWheelDelta:Int;
	private static var _touches:Map<Int,Touch>;
	private static var _joysticks:Map<Int,Joystick>;
	private static var _control:Map<String,Array<Int>>;
	private static var _nativeCorrection:Map<String, Int>;

  private static function __init__():Void
  {
    _enabled = false;
    _touchNum = 0;
    _key = new Map<Int, Bool>();
    _keyNum = 0;
    _press = new Array<Int>();
    _pressNum = 0;
    _textPress = new Array<Int>();
    _textPressNum = 0;
    _release = new Array<Int>();
    _releaseNum = 0;
    _mouseWheelDelta = 0;
    _touches = new Map<Int, Touch>();
    _joysticks = new Map<Int, Joystick>();
    _control = new Map<String, Array<Int>>();
    _nativeCorrection = new Map<String, Int>();
  }
  
	/**
	 * Contains the string of the last keys pressed
	 */
	public static var keyString:String = "";

	/**
	 * Holds the last key pressed
	 */
	public static var lastKey:Int;

  public static var lastCode:UInt;
  
	/**
	 * If the left button mouse is held down
	 */
	public static var mouseDown:Bool;
	/**
	 * If the left button mouse is up
	 */
	public static var mouseUp:Bool;
	/**
	 * If the left button mouse was recently pressed
	 */
	public static var mousePressed:Bool;
	/**
	 * If the left button mouse was recently released
	 */
	public static var mouseReleased:Bool;

#if !js
	/**
	 * If the right button mouse is held down
	 */
	public static var rightMouseDown:Bool;
	/**
	 * If the right button mouse is up
	 */
	public static var rightMouseUp:Bool;
	/**
	 * If the right button mouse was recently pressed
	 */
	public static var rightMousePressed:Bool;
	/**
	 * If the right button mouse was recently released
	 */
	public static var rightMouseReleased:Bool;

	/**
	 * If the middle button mouse is held down
	 */
	public static var middleMouseDown:Bool;
	/**
	 * If the middle button mouse is up
	 */
	public static var middleMouseUp:Bool;
	/**
	 * If the middle button mouse was recently pressed
	 */
	public static var middleMousePressed:Bool;
	/**
	 * If the middle button mouse was recently released
	 */
	public static var middleMouseReleased:Bool;
#end

	/**
	 * If the mouse wheel has moved
	 */
	public static var mouseWheel:Bool;

	/**
	 * Returns true if the device supports multi touch
	 */
	public static var multiTouchSupported(default, null):Bool = false;

	/**
	 * If the mouse wheel was moved this frame, this was the delta.
	 */
	public static var mouseWheelDelta(get, never):Int;
	public static function get_mouseWheelDelta():Int
	{
		if (mouseWheel)
		{
			mouseWheel = false;
			return _mouseWheelDelta;
		}
		return 0;
	}

	/**
	 * X position of the mouse on the screen.
	 */
	public static var mouseX(get, never):Int;
	private static inline function get_mouseX():Int
	{
		return GE.screen.mouseX;
	}

	/**
	 * Y position of the mouse on the screen.
	 */
	public static var mouseY(get, never):Int;
	private static inline function get_mouseY():Int
	{
		return GE.screen.mouseY;
	}
  
  public static var mouseXF(get, never):Float;
  private static inline function get_mouseXF():Float { return GE.screen.mouseXF; }
  public static var mouseYF(get, never):Float;
  private static inline function get_mouseYF():Float { return GE.screen.mouseYF; }

	/**
	 * The absolute mouse x position on the screen (unscaled).
	 */
	public static var mouseFlashX(get, never):Int;
	private static function get_mouseFlashX():Int
	{
		return Std.int(GE.stage.mouseX);
	}

	/**
	 * The absolute mouse y position on the screen (unscaled).
	 */
	public static var mouseFlashY(get, never):Int;
	private static function get_mouseFlashY():Int
	{
		return Std.int(GE.stage.mouseY);
	}

	/**
	 * Defines a new input.
	 * @param	name		String to map the input to.
	 * @param	keys		The keys to use for the Input.
	 */
	public static function define(name:String, keys:Array<Int>)
	{
		_control.set(name, keys);
	}
  
  public static function defineAxis(name:String, negativeKeys:Array<Int>, positiveKeys:Array<Int>):Void
  {
    _control.set("____AXIS_NEGATIVE_" + name, negativeKeys);
    _control.set("____AXIS_POSITIVE_" + name, positiveKeys);
  }

	/**
	 * If the input or key is held down.
	 * @param	input		An input name or key to check for.
	 * @return	True or false.
	 */
	public static function check(input:Dynamic):Bool
	{
		if (Std.is(input, String))
		{
#if debug
			if (!_control.exists(input))
			{
        trace("Input '" + input + "' not defined");
				return false;
			}
#end
			var v:Array<Int> = _control.get(input),
				i:Int = v.length;
			while (i-- > 0)
			{
				if (v[i] < 0)
				{
					if (_keyNum > 0) return true;
					continue;
				}
				if (_key[v[i]] == true) return true;
			}
			return false;
		}
		return input < 0 ? _keyNum > 0 : _key.get(input);
	}
  
  public static function axis(name:String):Int
  {
    var n:String = "____AXIS_NEGATIVE_" + name;
    if (!_control.exists(n))
    {
      trace("Axis '" + name + "' not defined");
      return 0;
    }
    
    return (check(n) ? -1 : 0) + (check("____AXIS_POSITIVE_" + name) ? 1 : 0);
  }

	/**
	 * If the input or key was pressed this frame.
	 * @param	input		An input name or key to check for.
	 * @return	True or false.
	 */
	public static function pressed(input:Dynamic):Bool
	{
		if (Std.is(input, String) && _control.exists(input))
		{
			var v:Array<Int> = _control.get(input),
				i:Int = v.length;
			while (i-- > 0)
			{
				if ((v[i] < 0) ? _pressNum != 0 : GEU.indexOf(_press, v[i]) >= 0) return true;
			}
			return false;
		}
		return (input < 0) ? _pressNum != 0 : GEU.indexOf(_press, input) >= 0;
	}
  
  public static function textPressed(input:Dynamic):Bool
  {
    if (Std.is(input, String) && _control.exists(input))
    {
      var v:Array<Int> = _control.get(input), i:Int = v.length;
      while (i-- > 0)
      {
        if ((v[i] < 0) ? _textPressNum != 0 : GEU.indexOf(_textPress, v[i]) >= 0) return true;
      }
      return false;
    }
    return (input < 0) ? _textPressNum != 0 : GEU.indexOf(_textPress, input) >= 0;
  }
  
	/**
	 * If the input or key was released this frame.
	 * @param	input		An input name or key to check for.
	 * @return	True or false.
	 */
	public static function released(input:Dynamic):Bool
	{
		if (Std.is(input, String))
		{
			var v:Array<Int> = _control.get(input),
				i:Int = v.length;
			while (i-- > 0)
			{
				if ((v[i] < 0) ? _releaseNum != 0 : GEU.indexOf(_release, v[i]) >= 0) return true;
			}
			return false;
		}
		return (input < 0) ? _releaseNum != 0 : GEU.indexOf(_release, input) >= 0;
	}
  
	public static function touchPoints(touchCallback:Touch->Void)
	{
		for (touch in _touches)
		{
			touchCallback(touch);
		}
	}

	public static var touches(get, never):Map<Int,Touch>;
	private static inline function get_touches():Map<Int,Touch> { return _touches; }

	/**
	 * Returns a joystick object (creates one if not connected)
	 * @param  id The id of the joystick, starting with 0
	 * @return    A Joystick object
	 */
	public static function joystick(id:Int):Joystick
	{
		var joy:Joystick = _joysticks.get(id);
		if (joy == null)
		{
			joy = new Joystick();
			_joysticks.set(id, joy);
		}
		return joy;
	}

	/**
	 * Returns the number of connected joysticks
	 */
	public static var joysticks(get, never):Int;
	private static function get_joysticks():Int
	{
		var count:Int = 0;
		for (joystick in _joysticks)
		{
			if (joystick.connected)
			{
				count += 1;
			}
		}
		return count;
	}

	/**
	 * Enables input handling
	 */
	public static function enable()
	{
		if (!_enabled && GE.stage != null)
		{
      _enabled = true;
			GE.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown, false,  2);
			GE.stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp, false,  2);
			GE.stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown, false,  2);
			GE.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp, false,  2);
			GE.stage.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel, false,  2);

		#if !js
			GE.stage.addEventListener(MouseEvent.MIDDLE_MOUSE_DOWN, onMiddleMouseDown, false, 2);
			GE.stage.addEventListener(MouseEvent.MIDDLE_MOUSE_UP, onMiddleMouseUp, false, 2);
      #if (!flash || flashRightClick)
        GE.stage.addEventListener(MouseEvent.RIGHT_MOUSE_DOWN, onRightMouseDown, false, 2);
        GE.stage.addEventListener(MouseEvent.RIGHT_MOUSE_UP, onRightMouseUp, false, 2);
      #end
		#end

			multiTouchSupported = Multitouch.supportsTouchEvents;
			if (multiTouchSupported)
			{
				Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;

				GE.stage.addEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
				GE.stage.addEventListener(TouchEvent.TOUCH_MOVE, onTouchMove);
				GE.stage.addEventListener(TouchEvent.TOUCH_END, onTouchEnd);
			}

#if (openfl && (cpp || neko))
			GE.stage.addEventListener(JoystickEvent.AXIS_MOVE, onJoyAxisMove);
			GE.stage.addEventListener(JoystickEvent.BALL_MOVE, onJoyBallMove);
			GE.stage.addEventListener(JoystickEvent.BUTTON_DOWN, onJoyButtonDown);
			GE.stage.addEventListener(JoystickEvent.BUTTON_UP, onJoyButtonUp);
			GE.stage.addEventListener(JoystickEvent.HAT_MOVE, onJoyHatMove);

		#if ouya
			// Initializing OuyaController
			var getContext = JNI.createStaticMethod("org.haxe.lime.GameActivity", "getContext", "()Landroid/content/Context;",true);
			OuyaController.init(getContext());
		#end
#end

		#if !(flash || js)
			_nativeCorrection.set("0_64", Key.INSERT);
			_nativeCorrection.set("0_65", Key.END);
			_nativeCorrection.set("0_66", Key.DOWN);
			_nativeCorrection.set("0_67", Key.PAGE_DOWN);
			_nativeCorrection.set("0_68", Key.LEFT);
			_nativeCorrection.set("0_69", -1);
			_nativeCorrection.set("0_70", Key.RIGHT);
			_nativeCorrection.set("0_71", Key.HOME);
			_nativeCorrection.set("0_72", Key.UP);
			_nativeCorrection.set("0_73", Key.PAGE_UP);
			_nativeCorrection.set("0_266", Key.DELETE);
			_nativeCorrection.set("123_222", Key.LEFT_SQUARE_BRACKET);
			_nativeCorrection.set("125_187", Key.RIGHT_SQUARE_BRACKET);
			_nativeCorrection.set("126_233", Key.TILDE);

			_nativeCorrection.set("0_80", Key.F1);
			_nativeCorrection.set("0_81", Key.F2);
			_nativeCorrection.set("0_82", Key.F3);
			_nativeCorrection.set("0_83", Key.F4);
			_nativeCorrection.set("0_84", Key.F5);
			_nativeCorrection.set("0_85", Key.F6);
			_nativeCorrection.set("0_86", Key.F7);
			_nativeCorrection.set("0_87", Key.F8);
			_nativeCorrection.set("0_88", Key.F9);
			_nativeCorrection.set("0_89", Key.F10);
			_nativeCorrection.set("0_90", Key.F11);

			_nativeCorrection.set("48_224", Key.DIGIT_0);
			_nativeCorrection.set("49_38", Key.DIGIT_1);
			_nativeCorrection.set("50_233", Key.DIGIT_2);
			_nativeCorrection.set("51_34", Key.DIGIT_3);
			_nativeCorrection.set("52_222", Key.DIGIT_4);
			_nativeCorrection.set("53_40", Key.DIGIT_5);
			_nativeCorrection.set("54_189", Key.DIGIT_6);
			_nativeCorrection.set("55_232", Key.DIGIT_7);
			_nativeCorrection.set("56_95", Key.DIGIT_8);
			_nativeCorrection.set("57_231", Key.DIGIT_9);

			_nativeCorrection.set("48_64", Key.NUMPAD_0);
			_nativeCorrection.set("49_65", Key.NUMPAD_1);
			_nativeCorrection.set("50_66", Key.NUMPAD_2);
			_nativeCorrection.set("51_67", Key.NUMPAD_3);
			_nativeCorrection.set("52_68", Key.NUMPAD_4);
			_nativeCorrection.set("53_69", Key.NUMPAD_5);
			_nativeCorrection.set("54_70", Key.NUMPAD_6);
			_nativeCorrection.set("55_71", Key.NUMPAD_7);
			_nativeCorrection.set("56_72", Key.NUMPAD_8);
			_nativeCorrection.set("57_73", Key.NUMPAD_9);
			_nativeCorrection.set("42_268", Key.NUMPAD_MULTIPLY);
			_nativeCorrection.set("43_270", Key.NUMPAD_ADD);
			//_nativeCorrection.set("", Key.NUMPAD_ENTER);
			_nativeCorrection.set("45_269", Key.NUMPAD_SUBTRACT);
			_nativeCorrection.set("46_266", Key.NUMPAD_DECIMAL); // point
			_nativeCorrection.set("44_266", Key.NUMPAD_DECIMAL); // comma
			_nativeCorrection.set("47_267", Key.NUMPAD_DIVIDE);
		#end
		}
	}

  public static function toggle(enabled:Bool)
  {
    _enabled = enabled;
    if (!enabled) releaseAllButtons();
  }
  
	/**
	 * Updates the input states
	 */
	public static function update()
	{
		while (_pressNum-- > -1) _press[_pressNum] = -1;
		_pressNum = 0;
		while (_releaseNum-- > -1) _release[_releaseNum] = -1;
		_releaseNum = 0;
    while (_textPressNum-- > -1) _textPress[_textPressNum] = -1;
    
		if (mousePressed) mousePressed = false;
		if (mouseReleased) mouseReleased = false;

	#if !js
		if (middleMousePressed) middleMousePressed = false;
		if (middleMouseReleased) middleMouseReleased = false;
		if (rightMousePressed) rightMousePressed = false;
		if (rightMouseReleased) rightMouseReleased = false;
	#end

#if (openfl && (cpp || neko))
		for (joystick in _joysticks) joystick.update();
#end
		if (multiTouchSupported)
		{
			for (touch in _touches) touch.update();
		}
	}

	private static function onKeyDown(e:KeyboardEvent = null)
	{
    if (!_enabled) return;
		var code:Int = keyCode(e);
		if (code == -1) // No key
			return;

		lastKey = code;
    lastCode = e.charCode;

		if (code == Key.BACKSPACE) keyString = keyString.substr(0, keyString.length - 1);
		else if ((code > 47 && code < 58) || (code > 64 && code < 91) || code == 32)
		{
			if (keyString.length > kKeyStringMax) keyString = keyString.substr(1);
			var char:String = String.fromCharCode(code);

			if (e.shiftKey != #if flash Keyboard.capsLock #else check(Key.CAPS_LOCK) #end)
				char = char.toUpperCase();
			else char = char.toLowerCase();

			keyString += char;
		}

		if (!_key[code])
		{
			_key[code] = true;
			_keyNum++;
			_press[_pressNum++] = code;
		}
    _textPress[_textPressNum++] = code;
	}

  public static function releaseAllButtons():Void
  {
    for (code in _key.keys())
    {
      if (_key[code])
      {
        _key[code] = false;
        _keyNum--;
        _release[_releaseNum++] = code;
      }
    }
  }
  
	private static function onKeyUp(e:KeyboardEvent = null)
	{
    if (!_enabled) return;
		var code:Int = keyCode(e);
		if (code == -1) // No key
			return;

		if (_key[code])
		{
			_key[code] = false;
			_keyNum--;
			_release[_releaseNum++] = code;
		}
	}

	public static function keyCode(e:KeyboardEvent) : Int
	{
	#if (flash || js)
		return e.keyCode;
	#else
		var code = _nativeCorrection.get(e.charCode + "_" + e.keyCode);

		if (code == null)
			return e.keyCode;
		else
			return code;
	#end
	}

	private static function onMouseDown(e:MouseEvent)
	{
		if (!mouseDown && _enabled)
		{
			mouseDown = true;
			mouseUp = false;
			mousePressed = true;
		}
	}

	private static function onMouseUp(e:MouseEvent)
	{
		mouseDown = false;
		mouseUp = true;
		mouseReleased = true;
	}

	private static function onMouseWheel(e:MouseEvent)
	{
		mouseWheel = true;
		_mouseWheelDelta = e.delta;
	}

#if !js
	private static function onMiddleMouseDown(e:MouseEvent)
	{
		if (!middleMouseDown && _enabled)
		{
			middleMouseDown = true;
			middleMouseUp = false;
			middleMousePressed = true;
		}
	}

	private static function onMiddleMouseUp(e:MouseEvent)
	{
		middleMouseDown = false;
		middleMouseUp = true;
		middleMouseReleased = true;
	}

	private static function onRightMouseDown(e:MouseEvent)
	{
		if (!rightMouseDown && _enabled)
		{
			rightMouseDown = true;
			rightMouseUp = false;
			rightMousePressed = true;
		}
	}

	private static function onRightMouseUp(e:MouseEvent)
	{
		rightMouseDown = false;
		rightMouseUp = true;
		rightMouseReleased = true;
	}
#end

	private static function onTouchBegin(e:TouchEvent)
	{
    // FIXME: Fix for offsetted Screen
		var touchPoint = new Touch(e.stageX / GE.screen.realScaleX, e.stageY / GE.screen.realScaleY, e.touchPointID);
		_touches.set(e.touchPointID, touchPoint);
		_touchNum += 1;
	}

	private static function onTouchMove(e:TouchEvent)
	{
    // FIXME: Fix for offsetted Screen
		var point = _touches.get(e.touchPointID);
		point.x = e.stageX / GE.screen.realScaleX;
		point.y = e.stageY / GE.screen.realScaleY;
	}

	private static function onTouchEnd(e:TouchEvent)
	{
		_touches.remove(e.touchPointID);
		_touchNum -= 1;
	}

#if (openfl && (cpp || neko))

	private static function onJoyAxisMove(e:JoystickEvent)
	{
	#if ouya
		var joy:Joystick = joystick(OuyaController.getPlayerNumByDeviceId(e.device));
	#else
		var joy:Joystick = joystick(e.device);
	#end

		joy.connected = true;
		joy.axis = e.axis;
	}

	private static function onJoyBallMove(e:JoystickEvent)
	{
	#if ouya
		var joy:Joystick = joystick(OuyaController.getPlayerNumByDeviceId(e.device));
	#else
		var joy:Joystick = joystick(e.device);
	#end

		joy.connected = true;
		joy.ball.x = (Math.abs(e.x) < Joystick.deadZone) ? 0 : e.x;
		joy.ball.y = (Math.abs(e.y) < Joystick.deadZone) ? 0 : e.y;
	}

	private static function onJoyButtonDown(e:JoystickEvent)
	{
	#if ouya
		var joy:Joystick = joystick(OuyaController.getPlayerNumByDeviceId(e.device));
	#else
		var joy:Joystick = joystick(e.device);
	#end
		joy.connected = true;
		joy.buttons.set(e.id, BUTTON_PRESSED);
	}

	private static function onJoyButtonUp(e:JoystickEvent)
	{
	#if ouya
		var joy:Joystick = joystick(OuyaController.getPlayerNumByDeviceId(e.device));
	#else
		var joy:Joystick = joystick(e.device);
	#end
		joy.connected = true;
		joy.buttons.set(e.id, BUTTON_RELEASED);
	}

	private static function onJoyHatMove(e:JoystickEvent)
	{
	#if ouya
		var joy:Joystick = joystick(OuyaController.getPlayerNumByDeviceId(e.device));
	#else
		var joy:Joystick = joystick(e.device);
	#end
		joy.connected = true;
		joy.hat.x = (Math.abs(e.x) < Joystick.deadZone) ? 0 : e.x;
		joy.hat.y = (Math.abs(e.y) < Joystick.deadZone) ? 0 : e.y;
	}

#end
}
