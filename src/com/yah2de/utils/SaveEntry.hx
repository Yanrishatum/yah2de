package com.yah2de.utils;

/**
 * ...
 * @author Yanrishatum
 */
class SaveEntry
{
  public var name:String;
  public var value:Dynamic;
  public var childs:Map<String, SaveEntry>;

  public function new(name:String, value:Dynamic) 
  {
    this.name = name;
    this.value = value;
    this.childs = new Map();
  }
  
  public function addChild(entry:SaveEntry):Void
  {
    childs.set(entry.name, entry);
  }
  
  public function child(name:String):SaveEntry
  {
    return childs.get(name);
  }
  
  public function addValue(name:String, value:Dynamic):Void
  {
    var entry:SaveEntry = new SaveEntry(name, value);
    childs.set(name, entry);
  }
  
  public function cvalue<T:Dynamic>(name:String):Null<T>
  {
    if (childs.exists(name))
    {
      return childs.get(name).value;
    }
    return null;
  }
  
  public function removeChildByName(name:String):Bool
  {
    if (this.childs.exists(name))
    {
      this.childs.remove(name);
      return true;
    }
    return false;
  }
  
  public function deleteChild(entry:SaveEntry):Bool
  {
    if (this.childs.exists(entry.name))
    {
      this.childs.remove(entry.name);
      return true;
    }
    return false;
  }
  
}