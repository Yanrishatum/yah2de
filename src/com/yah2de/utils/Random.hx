package com.yah2de.utils;

/**
 * Adapted from Nicolas Cannasse code.
 *
 * @author Franco Ponticelli
 * @author Nicolas Cannasse
 */

class Random
{
	public var seed : Int;
	
	public function new(seed = 1)
	{
		this.seed = seed;
	}
	
	public inline function int() : Int
	{
#if neko
		return untyped __dollar__int( seed = (seed * 16807.0) % 2147483647.0 ) & 0x3FFFFFFF;
#elseif flash9
		return (seed = Std.int((seed * 16807.0) % 2147483647.0)) & 0x3FFFFFFF;
#else
		return (seed = (seed * 16807) % 0x7FFFFFFF) & 0x3FFFFFFF;
#end
	}
  
  public inline function intRange(value:Int):Int
  {
    return Math.floor(value * float());
  }
  
  public inline function floatRange(value:Float):Float
  {
    return float() * value;
  }
  
  public inline function bool():Bool
  {
    return float() < 0.5;
  }
  
  public inline function chance(chance:Float):Bool
  {
    return float() < chance;
  }
  
	public inline function float() : Float
	{
		return int() / 1073741823.0; // divided by 2^30-1
	}
  
  public inline function pickRandom<T:Dynamic>(r:Array<T>):T
  {
    return r[intRange(r.length)];
  }
  
  public inline function random():Float
  {
    return float();
  }
  
  public static inline function random2(x:Int, y:Int, seed:Int):Int
  {
    x += y * 57;
    x = (x << 13) ^ x;
    #if neko
    return untyped __dollar__int((seed * x * 16807.0) % 2147483647.0 ) & 0x3FFFFFFF;
    #elseif flash9
    return Std.int((seed * x * 16807.0) % 2147483647.0 ) & 0x3FFFFFFF;
    #else
    return ((seed * x * 16807) % 2147483647 ) & 0x3FFFFFFF;
    #end
  }
  
  public static inline function random2f(x:Int, y:Int, seed:Int):Float
  {
    return random2(x, y, seed) / 1073741823.0;
  }
  
}