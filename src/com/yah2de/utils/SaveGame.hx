package com.yah2de.utils;
import openfl.net.SharedObject;

/**
 * ...
 * @author Yanrishatum
 */
class SaveGame
{

  public var entries:Map<String, SaveEntry>;
  
  public var independed:Map<String, Dynamic>;
  
  public function new() 
  {
    entries = new Map();
    independed = new Map();
  }
  
  public function addEntry(entry:SaveEntry):Void
  {
    entries.set(entry.name, entry);
  }
  
  public function removeEntry(entry:SaveEntry):Bool
  {
    if (entries.exists(entry.name))
    {
      entries.remove(entry.name);
      return true;
    }
    return false;
  }
  
  public function removeEntryByName(name:String):Bool
  {
    if (entries.exists(name))
    {
      entries.remove(name);
      return true;
    }
    return false;
  }
  
  public function entry(name:String):SaveEntry
  {
    return entries.get(name);
  }
  
  public function addValue(name:String, value:Dynamic):Void
  {
    independed.set(name, value);
  }
  
  public function removeValue(name:String):Bool
  {
    if (independed.exists(name))
    {
      independed.remove(name);
      return true;
    }
    return false;
  }
  
  public function value<T:Dynamic>(name:String):T
  {
    return cast independed.get(name);
  }
  
  public function load(name:String):Void
  {
    // TODO: Manual change protection?
    var sh:SharedObject = SharedObject.getLocal(name, "/");
    var fields:Array<String> = Reflect.fields(sh.data);
    for (field in fields)
    {
      var value:Dynamic = Reflect.field(sh.data, field);
      if (field == "____entries____")
      {
        var entriesArray:Array<Dynamic> = cast value;
        for (entryInfo in entriesArray)
        {
          var entry:SaveEntry = parseEntry(entryInfo);
          entries.set(entry.name, entry);
        }
      }
      else
      {
        independed.set(field, value);
      }
    }
  }
  
  public function save(name:String):Void
  {
    var sh:SharedObject = SharedObject.getLocal(name, "/");
    sh.clear();
    var out:Array<Dynamic> = new Array();
    for (entry in entries.iterator()) writeEntry(entry, out);
    sh.data.____entries____ = out;
    for (key in independed.keys())
    {
      Reflect.setField(sh.data, key, independed.get(key));
    }
    sh.flush();
  }
  
  private function writeEntry(entry:SaveEntry, to:Array<Dynamic>):Void
  {
    var out:Dynamic = { name:entry.name, value:entry.value };
    var childsOut:Array<Dynamic> = new Array();
    for (child in entry.childs.iterator())
    {
      writeEntry(child, childsOut);
    }
    if (childsOut.length > 0) out.childs = childsOut;
    to.push(out);
  }
  
  private function parseEntry(info:Dynamic):SaveEntry
  {
    var entry:SaveEntry = new SaveEntry(info.name, info.value);
    if (Reflect.hasField(info, "childs"))
    {
      var entries:Array<Dynamic> = info.childs;
      for (entryInfo in entries)
      {
        var child:SaveEntry = parseEntry(entryInfo);
        entry.childs.set(child.name, child);
      }
    }
    return entry;
  }
  
}