package com.yah2de;

/**
 * @author Yanrishatum
 */

enum MotionType 
{
  /**
   * Clears itself after finishing
   */
  OneShot;
  /**
   * Remains after finishing
   */
  Persistent;
  /**
   * Loops. In that case "finish" and "complete" will never be called.
   */
  Loop;
  /**
   * Bounces to target and back to zero, then finishes.
   */
  Bounce;
  /**
   * Same as Bounce, but clears itself after finishing.
   */
  BounceOneShot;
  /**
   * Loops Bounce behaviour.
   */
  BounceLoop;
  
}