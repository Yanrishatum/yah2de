package com.yah2de.visuals ;
import com.yah2de.Entity;
import openfl.display.BitmapData;
import openfl.display.Graphics;
import openfl.geom.Point;

/**
 * ...
 * @author Yanrishatum
 */
class Visual
{

  public var x:Float = 0;
  public var y:Float = 0;
  public var z:Float = 0;
  public var entity:Entity;
  public var visible:Bool = true;
  public var active:Bool = false;
  
  public function new(x:Float = 0, y:Float = 0, z:Float = 0) 
  {
    this.x = x;
    this.y = y;
    this.z = z;
  }
  
  public inline function disable():Void
  {
    this.active = this.visible = false;
  }
  
  public inline function enable():Void
  {
    this.active = this.visible = true;
  }
  
  public function added():Void
  {
    
  }
  
  public function removed():Void
  {
    
  }
  
  public function update():Void
  {
    
  }
  
  public function postUpdate():Void
  {
    
  }
  
  public function renderSoftware(buffer:BitmapData, camera:Point):Void
  {
    
  }
  
  public function renderHardware(g:Graphics, camera:Point, scaleX:Float, scaleY:Float):Void
  {
    
  }
  
}