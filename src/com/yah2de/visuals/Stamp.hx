package com.yah2de.visuals ;
import openfl.display.BitmapData;
import openfl.geom.Point;

/**
 * ...
 * @author Yanrishatum
 */
class Stamp extends Visual
{

  public var source:BitmapData;
  
  public function new(x:Float = 0, y:Float = 0, z:Float = 0, source:BitmapData = null) 
  {
    super(x, y, z);
    this.source = source;
  }
  
  override public function renderSoftware(buffer:BitmapData, camera:Point):Void 
  {
    if (source != null)
    {
      buffer.copyPixels(source, source.rect, new Point(entity.realX + this.x, (entity.realY - entity.realZ) + (this.y - this.z)), null, null, true);
    }
  }
  
}