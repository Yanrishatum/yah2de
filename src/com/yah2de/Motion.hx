package com.yah2de;
import com.yah2de.utils.Ease.EaseFunction;
import openfl.display.Graphics;

/**
 * ...
 * @author Yanrishatum
 */
class Motion extends Unit
{
  private var _type:MotionType;

  private var _target:Float = 0;
  public var targetInFrames:Bool = false;
  private var _time:Float = 0;
  private var _t:Float = 0;
  
  private var _subState:Int = 0;
  private var _reversing:Bool = false;
  
  public var target(get, set):Float;
  private inline function get_target():Float { return _target; }
  private inline function set_target(v:Float):Float
  {
    _target = v;
    _t = _time / _target;
    return _target;
  }
  
  public var targetObject:Dynamic;
  
  public var time(get, set):Float;
  private inline function get_time():Float { return _time; }
  private inline function set_time(v:Float):Float
  {
    if (v < 0) _time = _target + v;
    else _time = Math.min(v, _target);
    return _time;
  }
  
  public var t(get, set):Float;
  private inline function get_t():Float { return _t; }
  private inline function set_t(v:Float):Float
  {
    v = GEU.clamp(v, 1, -1);
    if (t < 0) _t = 1 + v;
    else _t = v;
    _time = _target * _t;
    return _t;
  }
  
  public var complete:Void -> Void;
  
  public var ease:EaseFunction;
  
  public function new() 
  {
    super();
    doRenderChilds = false;
    visible = false;
    active = false;
  }
  
  private inline function getTarget():Dynamic
  {
    return targetObject != null ? targetObject : parent;
  }
  
  override public function enable(andChilds:Bool = true, useValuesBeforeDisable:Bool = true):Void 
  {
    super.enable(andChilds, useValuesBeforeDisable);
    visible = false;
    doRenderChilds = false;
  }
  
  public function start(duration:Float, durationInFrames:Bool = false, motionType:MotionType = null, ease:EaseFunction = null):Void
  {
    if (motionType == null) motionType = MotionType.OneShot;
    _target = duration;
    this.ease = ease;
    targetInFrames = durationInFrames;
    _time = 0;
    _t = 0;
    _reversing = false;
    _type = motionType;
    applyMotion(_t);
    active = true;
  }
  
  override public function update():Void 
  {
    super.update();
    if (targetInFrames)
    {
      if (_reversing)
      {
        _time--;
        if (_time < 0) _time = 0;
      }
      else
      {
        _time++;
        if (_time > _target) _time = _target;
      }
    }
    else
    {
      if (_reversing)
      {
        _time -= GE.elapsed;
        if (_time < 0) _time = 0;
      }
      else
      {
        _time += GE.elapsed;
        if (_time > _target) _time = _target;
      }
    }
    
    _t = _time / _target;
    if (ease != null) _t = ease(_t);
    if (_time >= _target) _t = 1;
    applyMotion(_t);
    switch (_type)
    {
      case MotionType.OneShot:
        if (_time == _target) finish();
      case MotionType.Persistent:
        if (_time == _target) finish();
      case MotionType.Loop:
        if (_time == _target)
        {
          _time = 0;
          _t = 0;
        }
      case MotionType.Bounce, MotionType.BounceOneShot:
        if (_time == _target) _reversing = true;
        else if (_reversing && _time == 0) finish();
      case MotionType.BounceLoop:
        if (_time == _target) _reversing = true;
        else if (_reversing && _time == 0) _reversing = false;
    }
    
  }
  
  public function pause():Void
  {
    active = !active;
  }
  
  public function stop(callFinish:Bool = false):Void
  {
    active = false;
    _time = 0;
    _t = 0;
    _reversing = false;
    if (callFinish) finish();
  }
  
  private function finish():Void
  {
    if (complete != null) complete();
    active = false;
    if (_type == MotionType.OneShot || _type == MotionType.BounceOneShot)
    {
      complete = null;
      targetObject = null;
      parent.removeChild(this, false);
    }
  }
  
  public function applyMotion(t:Float):Void
  {
    
  }
  
}