package com.yah2de.motions;
import com.yah2de.Motion;
import com.yah2de.Unit;
import com.yah2de.GameEngineUtils.XYObject;
import com.yah2de.utils.Ease.EaseFunction;

/**
 * ...
 * @author Yanrishatum
 */
class LinearMotion extends Motion
{

  private var _targetX:Float;
  private var _targetY:Float;
  private var _fromX:Float;
  private var _fromY:Float;
  private var _toX:Float;
  private var _toY:Float;
  
  public var round:Bool;
  
  public function new() 
  {
    super();
  }
  
  public function setup(x:Float, y:Float, target:XYObject):Void
  {
    this.targetObject = target;
    this._toX = x;
    this._toY = y;
    this._fromX = target.x;
    this._fromY = target.y;
    this._targetX = _toX - _fromX;
    this._targetY = _toY - _fromY;
  }
  
  public function startSpeed(speed:Float, durationInFrames:Bool = false, motionType:MotionType = null, ease:EaseFunction = null):Void
  {
		start(Math.sqrt(GEU.square(_targetX) + GEU.square(_targetY)) / speed, durationInFrames, motionType, ease);
  }
  
  override public function applyMotion(t:Float):Void 
  {
    var target:XYObject = cast getTarget();
    if (round)
    {
      target.x = Math.round(_targetX * t + _fromX);
      target.y = Math.round(_targetY * t + _fromY);
    }
    else
    {
      target.x = _targetX * t + _fromX;
      target.y = _targetY * t + _fromY;
    }
  }
  
}