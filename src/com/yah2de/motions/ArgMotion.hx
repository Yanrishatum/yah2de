package com.yah2de.motions;
import com.yah2de.Motion;

/**
 * ...
 * @author Yanrishatum
 */
class ArgMotion extends Motion
{

  private var _activeTarget:Dynamic;
  private var _arg:String;
  private var _from:Float;
  private var _to:Float;
  private var _diff:Float;
  
  public function new() 
  {
    super();
  }
  
  public function setup(arg:String, from:Float, to:Float, target:Dynamic = null)
  {
    _activeTarget = target;
    _arg = arg;
    _from = from;
    _to = to;
    _diff = _to - _from;
  }
  
  override public function applyMotion(t:Float):Void 
  {
    var target:Dynamic = _activeTarget != null ? _activeTarget : parent;
    try
    {
      Reflect.setProperty(target, _arg, _diff * t + _from);
    }
    catch (e:Dynamic) { };
  }
  
}