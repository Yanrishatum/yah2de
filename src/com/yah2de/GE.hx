package com.yah2de;
import com.yah2de.colliders.BoxCollider;
import com.yah2de.utils.Input;
import haxe.Timer;
import openfl.geom.Point;
import openfl.display.BitmapData;
import openfl.display.Graphics;
import openfl.display.Stage;
import openfl.events.Event;
import openfl.geom.Rectangle;
import openfl.text.TextField;

/**
 * ...
 * @author Yanrishatum
 */
@:allow(com.yah2de.GECore)
class GE
{

  private static var _core:GECore;
  public static var core(get, never):GECore;
  private static inline function get_core():GECore { return _core; }
  
  public static var screen(get, never):Screen;
  private static inline function get_screen():Screen { return _core.screen; }
  
  public static var stage(get, never):Stage;
  private static inline function get_stage():Stage { return _core.stage; }
  
  public static var buffer(get, never):BitmapData;
  private static inline function get_buffer():BitmapData { return _core.screen._buffer; }
  
  /**
   * Please, do not change scene directly! Use GE.goto instead.
   */
  public static var scene(get, set):Scene;
  private static inline function get_scene():Scene { return _core.screen.scene; }
  private static inline function set_scene(v:Scene):Scene
  {
    _core.screen.scene = v; return v;
  }
  
  public static var width(get, set):Int;
  private static inline function get_width():Int { return _core.screen.get_width(); }
  private static inline function set_width(v:Int):Int { return _core.screen.set_width(v); }
  public static var height(get, set):Int;
  private static inline function get_height():Int { return _core.screen.get_height(); }
  private static inline function set_height(v:Int):Int { return _core.screen.set_height(v); }
  
  public static var realWidth(get, never):Float;
  private static inline function get_realWidth():Float { return _core.screen._width * _core.screen.get_realScaleX(); }
  public static var realHeight(get, never):Float;
  private static inline function get_realHeight():Float { return _core.screen._height * _core.screen.get_realScaleY(); }
  
  public static var halfWidth(get, never):Float;
  public static var halfHeight(get, never):Float;
  private static inline function get_halfWidth():Float { return _core.screen._halfWidth; }
  private static inline function get_halfHeight():Float { return _core.screen._halfHeight; }
  
  public static var renderMode(get, never):RenderMode;
  private static inline function get_renderMode():RenderMode { return screen._renderMode; }
  
  public static var isHardware(get, never):Bool;
  private static inline function get_isHardware():Bool { return get_renderMode() == RenderMode.Hardware; }
  
  public static var isBuffer(get, never):Bool;
  private static inline function get_isBuffer():Bool { return get_renderMode() == RenderMode.Buffer; }
  
  public static var point:Point = new Point();
  
  public static var rect:Rectangle = new Rectangle();
  private static var boxCollider:BoxCollider = new BoxCollider();
  private static var boxUnit:Unit = new Unit();
  
  public static var zero:Point = new Point();
  
  @:allow(com.yah2de.Screen)
  private static var coreReady:Bool = false;
  
  /**
   * How big elapsed value can become?
   * Note: This value don't caps time, modificated by timescale
   * Default: No limit
   */
  public static var limitElapsed:Float = Math.POSITIVE_INFINITY;
  /**
   * Use perfect elapsed time?
   * In this mode, time always will be perfect to this framerate (e.g. ~0.016 for 60 FPS)
   * Note: This value don't denies time, modificated by timescale.
   */
  public static var usePerfectElapsed:Bool = false;
  
  //{ Core init
  
  private static function startCoreInit(core:GECore, width:Int, height:Int, framerate:Float, renderMode:RenderMode, renderPriorityMode:RenderPriorityMode):Void
  {
    _core = core;
    _core.screen = new Screen(renderMode, renderPriorityMode, framerate, BufferingType.DoubleBuffer);
    _core.screen.resize(width, height, true);
    boxUnit.addCollider(boxCollider);
    if (_core.stage != null) onCoreAdded(null);
    else _core.addEventListener(Event.ADDED_TO_STAGE, onCoreAdded);
  }
  
  static private function onCoreAdded(e:Event):Void 
  {
    if (e != null) _core.removeEventListener(Event.ADDED_TO_STAGE, onCoreAdded);
    
    _core.stage.addEventListener(Event.RESIZE, onStageResized);
    screen.setStretch(true, true, false);
    #if ios
    Timer.delay(initCore, 100);
    #else
    initCore();
    #end
  }
  
  private static function initCore():Void
  {
    coreReady = true;
    Input.enable();
    _core.addChild(_core.screen.display);
    _core.init();
    screen.setStretch(screen._stretch, screen._keepAspectRatio, screen._pixelPerfect);
    stage.addEventListener(Event.DEACTIVATE, onDeactivate);
    stage.addEventListener(Event.ACTIVATE, onActivate);
    _core.addEventListener(Event.ENTER_FRAME, onGameTick);
  }
  
  //}
  
  private static var _goto:Scene;
  
  public static function goto(scene:Scene):Void
  {
    if (screen.scene == scene) return;
    _goto = scene;
  }
  
  private static var _lastTime:Float = Timer.stamp();
  
  private static var _elapsed:Float = 0;
  public static var elapsed(get, never):Float;
  private static inline function get_elapsed():Float { return _elapsed; }
  
  private static var _unscaledElapsed:Float = 0;
  public static var unscaledElapsed(get, never):Float;
  private static inline function get_unscaledElapsed():Float { return _unscaledElapsed; }
  
  public static var timescale:Float = 1;
  
  static private function onGameTick(e:Event):Void 
  {
    if (_goto != null)
    {
      screen.scene = _goto;
      _goto = null;
    }
    
    if (usePerfectElapsed)
    {
      _unscaledElapsed = 1 / stage.frameRate;
      _elapsed = _unscaledElapsed * timescale;
      _lastTime = Timer.stamp();
    }
    else
    {
      var s:Float = Timer.stamp();
      _elapsed = (s - _lastTime);
      if (_elapsed > limitElapsed) _elapsed = limitElapsed;
      _unscaledElapsed = _elapsed;
      _elapsed *= timescale;
      _lastTime = s;
    }
    
    if (_core.active)
    {
      _core.update();
      if (scene != null)
      {
        scene.update();
        scene.updateChilds();
      }
      
      for (updateFunction in updateFunctions)
      {
        updateFunction();
      }
      
      // Use stage.invalidate() and Event.RENDER?
      screen.render();
      if (screen._renderMode == RenderMode.Buffer)
      {
        screen.swap();
      }
    }
    Input.update();
    SoundManager.update();
    if (!Std.is(stage.focus, TextField)) 
    {
      stage.focus = stage;
    }
  }
  
  private static var _deactivated:Bool = false;
  
  static private function onDeactivate(e:Event):Void
  {
    if (_deactivated) return;
    _deactivated = true;
    _core.deactivate();
  }
  
  static private function onActivate(e:Event):Void
  {
    if (_deactivated)
    {
      _deactivated = false;
      _core.activate();
    }
  }
  
  static private function onStageResized(e:Event):Void 
  {
    var s:Screen = _core.screen;
    s.setStretch(s._stretch, s._keepAspectRatio, s._pixelPerfect);
    if (coreReady) _core.resize();
  }
  
  private static var updateFunctions:Array<Void->Void> = new Array();
  
  public static function addToUpdateLoop(fn:Void->Void):Void
  {
    if (updateFunctions.indexOf(fn) == -1) updateFunctions.push(fn);
  }
  
  public static function removeFromUpdateLoop(fn:Void->Void):Bool
  {
    return updateFunctions.remove(fn);
  }
  
}