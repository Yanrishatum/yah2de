package com.yah2de ;
import openfl.display.Graphics;
import openfl.geom.Point;
import com.yah2de.visuals.Visual;
import openfl.display.BitmapData;

/**
 * ...
 * @author Yanrishatum
 */
class Entity extends Unit
{
  
  private var _visuals:Array<Visual> = new Array();

  public function new() 
  {
    super();
  }
  
  public function addVisual<T:Visual>(v:T):T
  {
    if (v.entity == this) return v;
    else if (v.entity != null) v.entity.removeVisual(v);
    _visuals.push(v);
    v.entity = this;
    v.added();
    return v;
  }
  
  public function removeVisual(v:Visual):Void
  {
    if (v.entity == this)
    {
      _visuals.remove(v);
      v.removed();
      v.entity = null;
    }
  }
  
  override public function update():Void 
  {
    for (v in _visuals) if (v.active) v.update();
    super.update();
  }
  
  override public function postUpdate():Void 
  {
    for (v in _visuals) if (v.active) v.postUpdate();
    super.postUpdate();
  }
  
  override public function renderSoftware(buffer:BitmapData, camera:Point):Void 
  {
    for (v in _visuals) if (v.visible) v.renderSoftware(buffer, camera);
    super.renderSoftware(buffer, camera);
  }
  
  override public function renderHardware(g:Graphics, camera:Point, scaleX:Float, scaleY:Float):Void 
  {
    for (v in _visuals) if (v.visible) v.renderHardware(g, camera, scaleX, scaleY);
    super.renderHardware(g, camera, scaleX, scaleY);
  }
  
}