package com.yah2de;
import com.yah2de.GEU;
import openfl.Assets;
import openfl.events.Event;
import openfl.events.SampleDataEvent;
import openfl.media.Sound;
import openfl.media.SoundChannel;
import openfl.media.SoundTransform;
import openfl.utils.ByteArray;

typedef SoundEntry =
{
  var path:String;
  var name:String;
  var isMusic:Bool;
  var maxOnScene:Int;
}

/**
 * ...
 * @author Yanrishatum
 */
class SoundManager
{

  public static inline var HARDVARE_SFX_AMOUNT_LIMIT:Int = 5;
  
  private static var soundLibrary:Map<String, SoundInfo> = new Map();
  
  public static var sfxTransform:SoundTransform = new SoundTransform();
  public static var musicTransform:SoundTransform = new SoundTransform();
  private static var _sfxTransoform:SoundTransform = new SoundTransform();
  private static var _musicTransform:SoundTransform = new SoundTransform();
  
  private static var _muted:Bool = false;
  public static var muted(get, never):Bool;
  private static inline function get_muted():Bool { return _muted; }
  
  /**
   * Note: Give "path" in entries without extension!
   * For Flash or JS target will be choosed .mp3 extension.
   * For other platforms SFX will use .wav, and Music will use .ogg
   * @param sounds
   */
  public static function registerSounds(sounds:Array<SoundEntry>, autoselectExtension:Bool = true):Void
  {
    for (entry in sounds)
    {
      var sound:Sound;
      if (autoselectExtension)
      {
        #if (flash || js)
        sound = Assets.getSound(entry.path + ".mp3");
        #else
        if (entry.isMusic) sound = Assets.getMusic(entry.path + ".ogg");
        else sound = Assets.getSound(entry.path + ".wav");
        #end
      }
      else
      {
        sound = Assets.getSound(entry.path);
      }
      
      if (sound != null)
      {
        soundLibrary.set(entry.name, new SoundInfo(entry.name, sound, entry.maxOnScene, entry.isMusic));
      }
      else
      {
        #if yah2de_trace_SonudManager
        trace("Cannot find sound asset for '" + entry.name + "@" + entry.path + "'.");
        #end
      }
    }
  }
  
  public static function playSound(name:String):SoundChannel
  {
    var entry:SoundInfo = soundLibrary.get(name);
    if (entry != null)
    {
      return entry.playSFX();
    }
    else
    {
      #if yah2de_trace_SonudManager
      trace("Sound with name '" + name + "' not found in library!");
      #end
    }
    return null;
  }
  
  private static var _activeMusic:String;
  private static var _activeMusicInfo:SoundInfo;
  
  public static function playMusic(name:String, force:Bool = false):SoundChannel
  {
    var entry:SoundInfo = soundLibrary.get(name);
    if (entry != null)
    {
      if (_activeMusic != name || force)
      {
        if (_activeMusicInfo != null)
        {
          _activeMusicInfo.stopMusic();
        }
        _activeMusicInfo = entry;
        _activeMusic = name;
        return entry.playMusic();
      }
    }
    else
    {
      #if yah2de_trace_SonudManager
      trace("Music with name '" + name + "' not found!");
      #end
    }
    return null;
  }
  
  public static function mute():Void
  {
    _muted = !_muted;
    if (_muted)
    {
      for (s in soundLibrary.iterator()) s.mute();
      if (_activeMusicInfo != null) _activeMusicInfo.mute();
    }
    else
    {
      for (s in soundLibrary.iterator()) s.unmute();
      if (_activeMusicInfo != null) _activeMusicInfo.unmute();
    }
  }
  
  public static function update():Void
  {
    if (_sfxTransoform.volume != sfxTransform.volume || _sfxTransoform.pan != sfxTransform.pan)
    {
      _sfxTransoform.volume = sfxTransform.volume;
      _sfxTransoform.pan = sfxTransform.pan;
      if (!_muted)
      {
        for (s in soundLibrary.iterator())
        {
          if (!s.isMusic) s.update(sfxTransform);
        }
      }
    }
    if (_musicTransform.volume != musicTransform.volume || _musicTransform.pan != musicTransform.pan)
    {
      _musicTransform.volume = musicTransform.volume;
      _musicTransform.pan = musicTransform.pan;
      if (!_muted)
      {
        for (s in soundLibrary.iterator())
        {
          if (s.isMusic) s.update(musicTransform);
        }
      }
    }
  }
  
}



private class SoundInfo
{
  private static var _mutedTransform:SoundTransform = new SoundTransform(0, 0);
  
  public var name:String;
  public var sound:Sound;
  public var max:Int;
  public var isMusic:Bool;
  public var active:Int;
  public var channels:Array<SoundChannel>;
  
  #if yah2de_sound_timescale
  private var _samples:ByteArray;
  private var _sounds:Array<Sound>;
  
  private var _phases:Array<Float>;
  private var _numSamples:Int = 0;
  #end
  
  public function new(name:String, sound:Sound, max:Int, isMusic:Bool)
  {
    this.name = name;
    this.sound = sound;
    this.max = max;
    this.isMusic = isMusic;
    this.active = 0;
    this.channels = new Array();
    // TODO: Only for music
    #if yah2de_sound_timescale
    _sounds = new Array();
    _phases = new Array();
    _samples = new ByteArray();
    sound.extract(_samples, Std.int(sound.length * 44.1));
    _numSamples = Std.int(_samples.length / 8);
    #end
  }
  
  public function update(transform:SoundTransform):Void
  {
    if (active == 0) return;
    for (c in channels)
    {
      c.soundTransform = transform;
    }
  }
  
  public function mute():Void
  {
    if (active == 0) return;
    for (c in channels)
    {
      c.soundTransform = _mutedTransform;
    }
  }
  
  public function unmute():Void
  {
    if (active == 0) return;
    if (isMusic)
    {
      for (c in channels)
      {
        c.soundTransform = SoundManager.musicTransform;
      }
    }
    else
    {
      for (c in channels)
      {
        c.soundTransform = SoundManager.sfxTransform;
      }
    }
  }
  
  public function playSFX():SoundChannel
  {
    if (isMusic)
    {
      #if yah2de_trace_SonudManager
      trace("Can't play music as SFX!");
      #end
      return null;
    }
    if (max > 0 && (active == max || active == SoundManager.HARDVARE_SFX_AMOUNT_LIMIT))
    {
      return null; // Not allow new sound until old is done
      this.channels.shift().stop();
      this.active--;
    }
    var tr:SoundTransform = SoundManager.muted ? _mutedTransform : SoundManager.sfxTransform;
    #if yah2de_sound_timescale
    var snd:Sound = new Sound();
    snd.addEventListener(SampleDataEvent.SAMPLE_DATA, onSampleData);
    _phases.push(0);
    _sounds.push(snd);
    var channel:SoundChannel = snd.play(0, 0, tr);
    #else
    var channel:SoundChannel = sound.play(0, 0, tr);
    #end
    if (channel != null)
    {
      channel.addEventListener(Event.SOUND_COMPLETE, onSFXEnded);
      channels.push(channel);
      this.active++;
    }
    return channel;
  }
  
  public function playMusic():SoundChannel
  {
    if (!isMusic)
    {
      #if yah2de_trace_SonudManager
      trace("Can't play SFX as music!");
      #end
      return null;
    }
    var tr:SoundTransform = SoundManager.muted ? _mutedTransform : SoundManager.musicTransform;
    #if yah2de_sound_timescale
    var snd:Sound = new Sound();
    snd.addEventListener(SampleDataEvent.SAMPLE_DATA, onSampleData);
    _phases.push(0);
    _sounds.push(snd);
    var channel:SoundChannel = snd.play(0, 0, tr);
    channel.addEventListener(Event.SOUND_COMPLETE, onMusicEnded);
    channels.push(channel);
    
    #else
    var channel:SoundChannel = sound.play(0, 2147483647, tr);
    channel.addEventListener(Event.SOUND_COMPLETE, onMusicEnded);
    channels.push(channel);
    #end
    this.active++;
    return channel;
  }
  
  private function onMusicEnded(e:Event):Void 
  {
    this.active--;
    var c:SoundChannel = cast e.currentTarget;
    c.removeEventListener(Event.SOUND_COMPLETE, onMusicEnded);
    channels.remove(c);
    playMusic();
  }
  
  public function stopMusic(c:SoundChannel = null):Void
  {
    if (!isMusic) return;
    if (c == null)
    {
      this.active = 0;
      #if yah2de_sound_timescale
      for (sound in _sounds) sound.removeEventListener(SampleDataEvent.SAMPLE_DATA, onSampleData);
      GEU.clearArray(_sounds);
      GEU.clearArray(_phases);
      #end
      
      for (channel in channels)
      {
        channel.removeEventListener(Event.SOUND_COMPLETE, onMusicEnded);
        channel.stop();
      }
      GEU.clearArray(channels);
    }
    else
    {
      var idx:Int = channels.indexOf(c);
      if (idx != -1)
      {
        #if yah2de_sound_timescale
        var dyn:Sound = _sounds[idx];
        dyn.removeEventListener(SampleDataEvent.SAMPLE_DATA, onSampleData);
        _sounds.remove(dyn);
        _phases.splice(idx, 1);
        #end
        c.removeEventListener(Event.SOUND_COMPLETE, onMusicEnded);
        c.stop();
        channels.remove(c);
      }
      if (channels.remove(c))
      {
        c.removeEventListener(Event.SOUND_COMPLETE, onMusicEnded);
        c.stop();
      }
    }
  }
  
  private function onSFXEnded(e:Event):Void
  {
    this.active--;
    var c:SoundChannel = cast e.currentTarget;
    c.removeEventListener(Event.SOUND_COMPLETE, onSFXEnded);
    #if yah2de_sound_timescale
    var idx:Int = channels.indexOf(c);
    channels.remove(c);
    var snd:Sound = _sounds[idx];
    snd.removeEventListener(SampleDataEvent.SAMPLE_DATA, onSampleData);
    _sounds.remove(snd);
    _phases.splice(idx, 1);
    #else
    channels.remove(c);
    #end
  }
  
  #if yah2de_sound_timescale
  private function onSampleData(e:SampleDataEvent):Void
  {
    var idx:Int = _sounds.indexOf(e.currentTarget);
    if (idx == -1) return;
    var phase:Float = _phases[idx];
    var output:ByteArray = e.data;
    var outputLength:Int = 0;
    var prev:UInt = 1;
    
    var l:Float = 0;
    var r:Float = 0;
    while (outputLength < 2048)
    {
      // until we have filled up enough output buffer
      
      // move to the correct location in our loaded samples ByteArray
      _samples.position = Std.int(phase) * 8; // 4 bytes per float and two channels so the actual position in the ByteArray is a factor of 8 bigger than the phase
      
      // read out the left and right channels at this position
      if (prev != _samples.position)
      {
        l = _samples.readFloat();
        r = _samples.readFloat();
        prev = _samples.position;
      }
      
      // write the samples to our output buffer
      output.writeFloat(l);
      output.writeFloat(r);
      
      outputLength++;
      
      // advance the phase by the speed...
      phase += GE.timescale;
      
      // and deal with looping (including looping back past the beginning when playing in reverse)
      if (isMusic)
      {
        if (phase < 0) phase += _numSamples;
        else if (phase >= _numSamples) phase -= _numSamples;
      }
      else
      {
        if (phase >= _numSamples || phase < 0)
        {
          
          break; // SFX
        }
      }
    }
    _phases[idx] = phase;
  }
  #end
}
