package com.yah2de;
import openfl.display.Graphics;
import openfl.geom.Point;
import openfl.geom.Rectangle;

typedef ColliderChecker = Dynamic -> Bool;

/**
 * ...
 * @author Yanrishatum
 */
class Collider
{

  public var x:Float = 0;
  public var y:Float = 0;
  public var z:Float = 0;
  
  public var parent:Unit;
  
  public var active:Bool = true;
  public var needUpdate:Bool = false;
  
  private var _checks:Map<String, ColliderChecker> = new Map();
  private var _class:String;
  
  public var bounds:Rectangle;
  
  public var isTrigger:Bool = false;
  
  private var _p:Point;
  
  public var realX(get, never):Float;
  public var realY(get, never):Float;
  public var realZ(get, never):Float;
  private inline function get_realX():Float { return parent.realX + this.x; }
  private inline function get_realY():Float { return parent.realY + this.y; }
  private inline function get_realZ():Float { return parent.realZ + this.z; }
  
  public var onCollision:Unit -> Void;
  
  public function new(x:Float, y:Float, z:Float) 
  {
    this.x = x;
    this.y = y;
    this.z = z;
    _class = Type.getClassName(Type.getClass(this));
    bounds = new Rectangle(0, 0, 1, 1);
    _p = GE.point;
  }
  
  public function added():Void
  {
    
  }
  
  public function removed():Void
  {
    
  }
  
  public function update():Void
  {
    
  }
  
  public function postUpdate():Void
  {
    
  }
  
  private function updateBounds():Void
  {
    bounds.x = realX;
    bounds.y = realY;
  }
  
  public function renderDebug(g:Graphics):Void
  {
    
  }
  
  public inline function satisfiesCollideMode(mode:CollideMode):Bool
  {
    switch(mode)
    {
      case CollideMode.Physics: return !isTrigger;
      case CollideMode.Trigger: return isTrigger;
      default: return true;
    }
  }
  
  public inline function canCollideWith(other:Collider, mode:CollideMode):Bool
  {
    if (mode == CollideMode.Both) return true;
    else return other.isTrigger == this.isTrigger && ((mode == CollideMode.Physics && !isTrigger) || (mode == CollideMode.Trigger && isTrigger));
  }
  
  public function collides(other:Collider):Bool
  {
    var z:Float = Math.abs((other.realZ) - (this.realZ));
    if (!active || !other.active || z > this.parent.zCollideThreshold || z > other.parent.zCollideThreshold) return false;
    
    updateBounds();
    other.updateBounds();
    if (!bounds.intersects(other.bounds)) return false;
    
    var checker:ColliderChecker = _checks.get(other._class);
    if (checker != null)
    {
      var c:Bool = checker(other);
      if (c) 
      {
        if (onCollision != null) onCollision(other.parent);
        if (other.onCollision != null) other.onCollision(parent);
      }
      return c;
    }
    
    checker = other._checks.get(_class);
    if (checker != null)
    {
      var c:Bool = checker(this);
      if (c)
      {
        if (onCollision != null) onCollision(other.parent);
        if (other.onCollision != null) other.onCollision(parent);
      }
      return c;
    }
    
    trace("This two classes doesn't have a collision function! " + _class + " <=> " + other._class);
    
    return false;
  }
  
}