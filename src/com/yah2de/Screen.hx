package com.yah2de ;
import com.yah2de.Scene;
import com.yah2de.Unit;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Graphics;
import openfl.display.Shape;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.events.TimerEvent;
import openfl.geom.Point;
import openfl.geom.Rectangle;
import openfl.Lib;
import openfl.utils.Timer;

/**
 * ...
 * @author Yanrishatum
 */
@:allow(com.yah2de.GE)
class Screen
{
  private var _width:Int = 0;
  private var _height:Int = 0;
  private var _halfWidth:Float = 0;
  private var _halfHeight:Float = 0;
  private var _scale:Float = 1;
  private var _scaleX:Float = 1;
  private var _scaleY:Float = 1;
  
  private var _bounds:Rectangle;
  
  private var _stretch:Bool;
  private var _keepAspectRatio:Bool;
  private var _pixelPerfect:Bool;
  
  public var width(get, set):Int;
  private inline function get_width():Int { return _width; }
  private inline function set_width(v:Int):Int
  {
    resize(v, _height);
    return _width;
  }
  
  public var height(get, set):Int;
  private inline function get_height():Int { return _height; }
  private inline function set_height(v:Int):Int
  {
    resize(_width, v);
    return _height;
  }
  
  public var halfWidth(get, never):Float;
  private inline function get_halfWidth():Float { return _halfWidth; }
  
  public var halfHeight(get, never):Float;
  private inline function get_halfHeight():Float { return _halfHeight; }
  
  public var scale(get, set):Float;
  private inline function get_scale():Float { return _scale; }
  private inline function set_scale(v:Float):Float
  {
    setScale(v, _scaleX, _scaleY);
    return _scale;
  }
  
  public var scaleX(get, set):Float;
  private inline function get_scaleX():Float { return _scaleX; }
  private inline function set_scaleX(v:Float):Float
  {
    setScale(_scale, v, _scaleY);
    return _scaleX;
  }
  
  public var scaleY(get, set):Float;
  private inline function get_scaleY():Float { return _scaleY; }
  private inline function set_scaleY(v:Float):Float
  {
    setScale(_scale, _scaleX, v);
    return _scaleY;
  }
  
  public var realScaleX(get, never):Float;
  private inline function get_realScaleX():Float { return this.display.scaleX; }
  public var realScaleY(get, never):Float;
  private inline function get_realScaleY():Float { return this.display.scaleY; }
  
  public var stretch(get, set):Bool;
  private inline function get_stretch():Bool { return _stretch; }
  private inline function set_stretch(v:Bool):Bool
  {
    setStretch(v, _keepAspectRatio, _pixelPerfect);
    return _stretch;
  }
  
  public var keepAspectRatio(get, set):Bool;
  private inline function get_keepAspectRatio():Bool { return _keepAspectRatio; }
  private inline function set_keepAspectRatio(v:Bool):Bool
  {
    setStretch(_stretch, v, _pixelPerfect);
    return _keepAspectRatio;
  }
  
  public var pixelPerfect(get, set):Bool;
  private inline function get_pixelPerfect():Bool { return _pixelPerfect; }
  private inline function set_pixelPerfect(v:Bool):Bool
  {
    setStretch(_stretch, _keepAspectRatio, v);
    return _pixelPerfect;
  }
  
  public var mouseX(get, never):Int;
  public var mouseY(get, never):Int;
  private inline function get_mouseX():Int { return Std.int(this.display.mouseX); }
  private inline function get_mouseY():Int { return Std.int(this.display.mouseY); }
  
  public var mouseXF(get, never):Float;
  public var mouseYF(get, never):Float;
  private inline function get_mouseXF():Float { return this.display.mouseX; }
  private inline function get_mouseYF():Float { return this.display.mouseY; }
  
  private var _renderMode:RenderMode;
  private var _priorityMode:RenderPriorityMode;
  private var _framerate:Float;
  
  private var _currentDisplay:Int = 0;
  private var _buffersCount:Int = 2;
  private var _bufferingType:BufferingType;
  
  private var _buffers:Array<BitmapData>;
  private var _displays:Array<Bitmap>;
  private var _buffer:BitmapData;
  
  public var bufferingType(get, set):BufferingType;
  private inline function get_bufferingType():BufferingType { return _bufferingType; }
  private inline function set_bufferingType(v:BufferingType):BufferingType
  {
    _bufferingType = v;
    recreateBuffers();
    return _bufferingType;
  }
  
  public var renderMode(get, set):RenderMode;
  private inline function get_renderMode():RenderMode { return _renderMode; }
  private inline function set_renderMode(v:RenderMode):RenderMode
  {
    _renderMode = v;
    recreateBuffers();
    return _renderMode;
  }
  
  private var _offscreenMask:Shape;
  public var display:Sprite;
  public var buffersDisplay:Sprite;
  public var gui:Sprite;
  
  public var backgroundColor:Int = 0xFFFFFF;
  public var backgroundAlpha:Float = 1;
  
  private var _scene:Scene;
  public var scene(get, set):Scene;
  private inline function get_scene():Scene { return _scene; }
  private function set_scene(v:Scene):Scene
  {
    if (v == null) return _scene;
    if (v == _scene) return v;
    
    if (_scene != null)
    {
      if (_scene.sprite.parent == buffersDisplay) buffersDisplay.removeChild(_scene.sprite);
      if (_scene.gui.parent == gui) gui.removeChild(_scene.gui);
      _scene.deactivated();
    }
    _scene = v;
    buffersDisplay.addChild(v.sprite);
    gui.addChild(v.gui);
    
    var g:Graphics = v.spriteMask.graphics;
    g.clear();
    g.beginFill(0, 0);
    g.drawRect(0, 0, _width, _height);
    g.endFill();
    
    v.activated();
    
    return v;
  }
  
  public function new(mode:RenderMode, priorityMode:RenderPriorityMode, framerate:Float, bufferType:BufferingType) 
  {
    Lib.current.stage.frameRate = framerate;
    this._framerate = framerate;
    this._renderMode = mode;
    this._priorityMode = priorityMode;
    
    this.display = new Sprite();
    
    this._offscreenMask = new Shape();
    this.display.mask = this._offscreenMask;
    this.display.addChild(this._offscreenMask);
    
    this.buffersDisplay = new Sprite();
    this.display.addChild(buffersDisplay);
    
    this.gui = new Sprite();
    this.display.addChild(gui);
    
    _buffers = new Array();
    _displays = new Array();
    this.set_bufferingType(bufferType);
  }
  
  private function recreateBuffers():Void
  {
    for (buffer in _buffers) buffer.dispose();
    for (display in _displays) this.buffersDisplay.removeChild(display);
    _buffer = null;
    GameEngineUtils.clearArray(_buffers);
    GameEngineUtils.clearArray(_displays);
    
    if (_bufferingType == BufferingType.SingleBuffer) _buffersCount = 1;
    else if (_bufferingType == BufferingType.TripleBuffer) _buffersCount = 3;
    else _buffersCount = 2;
    
    for (i in 0..._buffersCount)
    {
      var display:Bitmap = new Bitmap();
      this.buffersDisplay.addChild(display);
      _displays.push(display);
    }
    
    if (GE.coreReady)
    {
      resize(_width, _height, true);
    }
  }
  
  private function swap():Void 
  {
    if (_renderMode == RenderMode.Buffer)
    {
      _displays[_currentDisplay].visible = false;
      _currentDisplay = (_currentDisplay + 1) % _buffersCount;
      _displays[_currentDisplay].visible = true;
      _buffer = _buffers[(_currentDisplay + 1) % _buffersCount];
    }
  }
  
  public function render():Void
  {
    if (_renderMode == RenderMode.Buffer)
    {
      this.display.graphics.clear();
      this.display.graphics.beginFill(backgroundColor, backgroundAlpha);
      this.display.graphics.drawRect(0, 0, this._width, this._height);
      this.display.graphics.endFill();
      _buffer.fillRect(_bounds, 0);
    }
    
    if (scene != null)
    {
      if (_renderMode == RenderMode.Hardware)
      {
        this.scene.sprite.graphics.clear();
        this.scene.sprite.graphics.beginFill(backgroundColor, backgroundAlpha);
        this.scene.sprite.graphics.drawRect(0, 0, this._width * realScaleX, this._height * realScaleY);
        this.scene.sprite.graphics.endFill();
      }
      if (_priorityMode == RenderPriorityMode.Hierarchy) renderHierarchy();
      else renderHeap();
    }
  }
  
  //{ Rendering by priority type
  
  //{ Heap/HeapRealY
  
  private var _renderFirst:Unit;
  
  private function renderHeap():Void
  {
    if (scene.visible)
    {
      _renderFirst = scene;
      _renderFirst.renderNext = null;
    }
    else _renderFirst = null;
    _heapAddChilds(scene);
    
    #if yah2de_render_colliders
    this.gui.graphics.clear();
    #end
    
    // Check mode out of loop for faster execution
    if (_renderMode == RenderMode.Buffer)
    {
      var u:Unit = _renderFirst;
      while (u != null)
      {
        u.renderSoftware(_buffer, scene.camera);
        #if yah2de_render_colliders
        u.renderColliders(this.gui.graphics);
        #end
        u = u.renderNext;
      }
    }
    else
    {
      var rsx:Float = realScaleX;
      var rsy:Float = realScaleY;
      var u:Unit = _renderFirst;
      while (u != null)
      {
        u.renderHardware(scene.sprite.graphics, scene.camera, rsx, rsy);
        #if yah2de_render_colliders
        u.renderColliders(this.gui.graphics);
        #end
        u = u.renderNext;
      }
    }
  }
  
  private function _heapAddChilds(u:Unit):Void
  {
    if (u.doRenderChilds)
    {
      u = u._childFirst;
      while (u != null)
      {
        _heapAddChild(u);
        u = u.next;
      }
    }
  }
  
  private function _heapAddChild(u:Unit):Void
  {
    _heapAddChilds(u);
    if (u.visible)
    {
      var curr:Unit = _renderFirst;
      var prev:Unit = null;
      var uy:Float = u.realY;
      var isRealY:Bool = _priorityMode.equals(RenderPriorityMode.HeapRealY);
      u.renderNext = null;
      
      // W
      if (isRealY)
      {
        while (curr != null)
        {
          if (u.w >= curr.w) break;
          prev = curr;
          curr = curr.renderNext;
        }
        
        while (curr != null)
        {
          if (u.w < curr.w || uy <= curr.realY)
          {
            u.renderNext = curr;
            if (prev != null) prev.renderNext = u;
            else _renderFirst = u;
            return;
          }
          prev = curr;
          curr = curr.renderNext;
        }
      }
      else
      {
        var uw:Float = u.realW;
        while (curr != null)
        {
          if (uw <= curr.realW) break;
          prev = curr;
          curr = curr.renderNext;
        }
        
        while (curr != null)
        {
          if (uw < curr.realW || uy <= curr.realY)
          {
            u.renderNext = curr;
            if (prev != null) prev.renderNext = u;
            else _renderFirst = u;
            return;
          }
          prev = curr;
          curr = curr.renderNext;
        }
      }
      
      if (curr == null) // All units have lower priority
      {
        if (prev != null) prev.renderNext = u;
        else _renderFirst = u;
      }
    }
  }
  
  //}
  
  // Hierarchy
  private inline function renderHierarchy():Void
  {
    if (_renderMode == RenderMode.Buffer)
    {
      scene.renderSoftware(_buffer, scene.camera);
      scene.renderChildsSoftware(_buffer, scene.camera);
    }
    else
    {
      scene.renderHardware(scene.sprite.graphics, scene.camera, realScaleX, realScaleY);
      scene.renderChildsHardware(scene.sprite.graphics, scene.camera, realScaleX, realScaleY);
    }
  }
  
  //}
  
  //{ Positioning and scaling
  
  /**
   * Resizes screen to desired size.
   * @param width Width of Screen in pixels. If size less than or equals 0, size will be stageWidth - Abs(size)
   * @param height Height of Screen in pixels. If size less than or equals 0, size will be stageWidth - Abs(size)
   * @param force Force resize window? If set to true, all buffers will be cleared, even if width/height remains same.
   */
  public function resize(width:Int, height:Int, force:Bool = false):Void
  {
    if (!force && width == _width && height == _height) return; // Already that size, buffers created
    _width  = width  == 0 ? Lib.current.stage.stageWidth  : (width  < 0 ? Lib.current.stage.stageWidth  + width  : width );
    _height = height == 0 ? Lib.current.stage.stageHeight : (height < 0 ? Lib.current.stage.stageHeight + height : height);
    _halfWidth = _width / 2;
    _halfHeight = _height / 2;
    _bounds = new Rectangle(0, 0, _width, _height);
    
    GameEngineUtils.clearArray(_buffers);
    for (i in 0..._buffersCount)
    {
      if (_renderMode == RenderMode.Buffer)
      {
        var disp:Bitmap = _displays[i];
        disp.visible = false;
        if (disp.bitmapData != null) disp.bitmapData.dispose();
        var data:BitmapData = new BitmapData(_width, _height, false);
        _buffers.push(data);
        disp.bitmapData = data;
      }
    }
    
    _currentDisplay = _buffersCount - 1;
    if (_renderMode == RenderMode.Buffer)
    {
      _buffer = _buffers[0];
      _displays[0].visible = true;
    }
    
    // Draw mask
    this._offscreenMask.graphics.clear();
    this._offscreenMask.graphics.beginFill(0, 0);
    this._offscreenMask.graphics.drawRect(0, 0, width, height);
    this._offscreenMask.graphics.endFill();
    
    if (_scene != null)
    {
      var g:Graphics = _scene.spriteMask.graphics;
      g.clear();
      g.beginFill(0, 0);
      g.drawRect(0, 0, _width, _height);
      g.endFill();
    }
    
    setStretch(_stretch, _keepAspectRatio, _pixelPerfect);
  }
  
  /**
   * Offsets Screen position.
   * @param x X offset of Screen
   * @param y Y offset of Screen
   * @param percentage Is offset represented in percents?
   * @param relativeToCenter Is offset represents center of Screen?
   */
  public function offset(x:Float, y:Float, percentage:Bool = false, relativeToCenter:Bool = false):Void
  {
    if (percentage)
    {
      x = x / 100 * Lib.current.stage.stageWidth;
      y = y / 100 * Lib.current.stage.stageHeight;
    }
    if (relativeToCenter)
    {
      x -= (this._width * this.display.scaleX) / 2;
      y -= (this._height * this.display.scaleY) / 2;
    }
    this.display.x = Math.round(x);
    this.display.y = Math.round(y);
  }
  
  /**
   * Sets custom size of window
   * Real scale calculates as scaleX/scaleY * scale.
   * @param scaleX Horizontal scale of Screen.
   * @param scaleY Vertical scale of Screen.
   * @param scale Overall scale of Screen.
   */
  public function setScale(scale:Float = 1, scaleX:Float = 1, scaleY:Float = 1):Void
  {
    if (scaleX <= 0) scaleX = 1;
    if (scaleY <= 0) scaleY = 1;
    if (scale <= 0) scale = 1;
    
    _scale = scale;
    _scaleX = scaleX;
    _scaleY = scaleY;
    _stretch = false;
    
    this.display.scaleX = scaleX * scale;
    this.display.scaleY = scaleY * scale;
  }
  
  /**
   * Sets stretching mode of Screen.
   * If stretch is true, Screen automatically resizes, relative to actual screen size.
   * @param stretch Enable stretch?
   * Default: true
   * @param keepAspectRatio Kepp aspect ratio of Screen? If set to true, Screen will kepp desired aspect ratio, and don't stretch to full window size.
   * Default: true
   * @param pixelPerfect Stretch Screen to keep pixels always be square? Works only when keepAspectRatio is true.
   * Default: false
   */
  public function setStretch(stretch:Bool = true, keepAspectRatio:Bool = true, pixelPerfect:Bool = false):Void
  {
    _stretch = stretch;
    _keepAspectRatio = keepAspectRatio;
    _pixelPerfect = pixelPerfect;
    if (!_stretch)
    {
      setScale(_scaleX, _scaleY, _scale);
    }
    else
    {
      if (keepAspectRatio)
      {
        var scale:Float = Math.min(Lib.current.stage.stageWidth / this._width, Lib.current.stage.stageHeight / this._height);
        if (pixelPerfect) scale = Math.floor(scale);
        this.display.scaleX = scale;
        this.display.scaleY = scale;
      }
      else
      {
        this.display.scaleX = Lib.current.stage.stageWidth / this._width;
        this.display.scaleY = Lib.current.stage.stageHeight / this._height;
      }
    }
  }
  
  //}
  
}