package com.yah2de.colliders;

/**
 * ...
 * @author Yanrishatum
 */
class CenterBoxCollider extends BoxCollider
{

  public function new(x:Float = 0, y:Float = 0, width:Float = 1, height:Float = 1, z:Float = 0) 
  {
    super(x, y, width, height, z);
    _checks.set(_class, checkBoxCollide);
  }
  
  override function updateBounds():Void 
  {
    var hw:Float = this.width / 2;
    var hh:Float = this.height / 2;
    bounds.x = realX - hw;
    bounds.y = realY - hh;
    bounds.width = this.width;
    bounds.height = this.height;
  }
  
}