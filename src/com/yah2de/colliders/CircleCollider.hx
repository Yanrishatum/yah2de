package com.yah2de.colliders;
import com.yah2de.Collider;
import openfl.display.Graphics;

/**
 * ...
 * @author Yanrishatum
 */
class CircleCollider extends Collider
{

  public var radius(get, set):Float;
  private inline function get_radius():Float { return _radius; }
  private inline function set_radius(v:Float):Float
  {
    _radius = v;
    _radius2 = v + v;
    _radiusSquare = v * v;
    return v;
  }
  private var _radius:Float;
  private var _radius2:Float;
  private var _radiusSquare:Float;
  
  public function new(x:Float = 0, y:Float = 0, radius:Float = 1, z:Float = 0) 
  {
    this.radius = radius;
    super(x, y, z);
    _checks.set(_class, checkCircleCollide);
    _checks.set(Type.getClassName(BoxCollider), checkBoxCollide);
    _checks.set(Type.getClassName(CenterBoxCollider), checkBoxCollide);
  }
  
  private function checkCircleCollide(c:CircleCollider):Bool
  {
		var dx:Float = realX - c.realX;
		var dy:Float = realY - c.realY;
		return (dx * dx + dy * dy) < GEU.square(_radius + c._radius);//Math.pow(_radius + c._radius, 2);
  }
  
  private function checkBoxCollide(b:BoxCollider):Bool
  {
    var hw:Float = b.halfWidth;
    var hh:Float = b.halfHeight;
    
    var px:Float = realX;
    var py:Float = realY;
    
    var distanceX:Float = Math.abs(px - b.bounds.x - hw);
    var distanceY:Float = Math.abs(py - b.bounds.y - hh);
    
    // Check for distance not required, because we already checked AABB
    //if (distanceX > hw + _radius || distanceY > hh + _radius) return false; // Too far
    
    if (distanceX <= hw || distanceY <= hh) return true;
    
    return GEU.square(distanceX - hw) + GEU.square(distanceY - hh) <= _radiusSquare;
  }
  
  override function updateBounds():Void 
  {
    bounds.x = this.x + parent.realX - _radius;
    bounds.y = this.y + parent.realY - _radius;
    bounds.width = _radius2;
    bounds.height = _radius2;
  }
  
  override public function renderDebug(g:Graphics):Void 
  {
    super.renderDebug(g);
    g.lineStyle(0.1, 0x00FF00);
    g.drawCircle(realX, realY, _radius);
    g.lineStyle();
  }
  
}