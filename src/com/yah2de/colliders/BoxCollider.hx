package com.yah2de.colliders;
import com.yah2de.Collider;
import openfl.display.Graphics;
import openfl.geom.Rectangle;

/**
 * ...
 * @author Yanrishatum
 */
class BoxCollider extends Collider
{

  public var width(get, set):Float;
  public var height(get, set):Float;
  private inline function get_width():Float { return _width; }
  private inline function get_height():Float { return _height; }
  private inline function set_width(v:Float):Float
  {
    _width = v;
    _halfWidth = v * 0.5;
    return v;
  }
  private inline function set_height(v:Float):Float
  {
    _height = v;
    _halfHeight = v * 0.5;
    return v;
  }
  private var _width:Float;
  private var _height:Float;
  private var _halfWidth:Float;
  private var _halfHeight:Float;
  public var halfWidth(get, never):Float;
  public var halfHeight(get, never):Float;
  private inline function get_halfWidth():Float { return _halfWidth; }
  private inline function get_halfHeight():Float { return _halfHeight; }
  
  public function new(x:Float = 0, y:Float = 0, width:Float = 1, height:Float = 1, z:Float = 0)
  {
    super(x, y, z);
    this.width = width;
    this.height = height;
    _checks.set(_class, checkBoxCollide);
  }
  
  override function updateBounds():Void 
  {
    super.updateBounds();
    bounds.width = this.width;
    bounds.height = this.height;
  }
  
  private function checkBoxCollide(b:BoxCollider):Bool
  {
    return return true; // Bounds already overlaps
  }
  
  override public function renderDebug(g:Graphics):Void 
  {
    super.renderDebug(g);
    g.lineStyle(0.1, 0xFF0000);
    g.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
    g.lineStyle();
  }
  
}