package com.yah2de ;

/**
 * ...
 * @author Yanrishatum
 */
enum RenderMode
{

  /**
   * Use OpenGL to render image.
   */
  Hardware;
  /**
   * Use two BitmapDatas to render image
   */
  Buffer;
  
}