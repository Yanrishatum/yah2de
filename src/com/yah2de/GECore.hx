package com.yah2de;
import openfl.display.Sprite;
import openfl.events.Event;

/**
 * ...
 * @author Yanrishatum
 */
@:allow(com.yah2de.GE)
class GECore extends Sprite
{

  public var screen:Screen;
  public var active:Bool = true;
  public var motionsUnit:Unit;
  
  public function new(width:Int, height:Int, framerate:Float = 60, renderMode:RenderMode = null, renderPriorityMode:RenderPriorityMode = null) 
  {
    if (renderMode == null) renderMode = RenderMode.Buffer;
    if (renderPriorityMode == null) renderPriorityMode = RenderPriorityMode.Heap;
    
    super();
    motionsUnit = new Unit();
    GE.startCoreInit(this, width, height, framerate, renderMode, renderPriorityMode);
  }
  
  public function update():Void
  {
    motionsUnit.updateChilds();
  }
  
  public function init():Void
  {
    
  }
  
  public function deactivate():Void
  {
    
  }
  
  public function activate():Void
  {
    
  }
  
  public function resize():Void
  {
    
  }
  
}